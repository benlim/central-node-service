const lib = require('./lib/lib.js');
const host = `${lib.CONFIG.IP}:${lib.CONFIG.PORT}`;

module.exports = {
  definition: {
    openapi: '3.0.0', // Specification (optional, defaults to swagger: '2.0')
    info: {
      // API informations (required)
      title: 'Hello World', // Title (required)
      version: '1.0.0', // Version (required)
      description: 'A sample API' // Description (optional)
    }
  },
  host, // Host (optional)
  basePath: lib.CONFIG.PREFIX // Base path (optional)
};
