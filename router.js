const lib = require('./lib/lib.js');
const utility = require('./lib/utility.js');
const router = require('express').Router();

const users = require('./lib/api/users.js');
const wallet = require('./lib/api/wallet.js');
const watchAddress = require('./lib/api/watchAddress.js');
const testIPN = require('./lib/tests/ipn_listener.js');

router.route('/import/transaction')
  .post(function (req, res) {
    wallet.importTransaction(req, res);
  });

// TODO: Add verbosity switch for all console logs.
router.use(async function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');

  var apiKey = req.headers.apikey || null;
  var hmac = req.headers.hmac || null;
  var body = '';
  if (req.body) {
    body = req.body;
  }

  var skip = false;
  var originalUrl = req.originalUrl;
  // var endpoint = req._parsedUrl.path
  // var method = req.method
  // var epoch = Math.floor(new Date())
  // var ipAddr = req.headers['x-forwarded-for'] || req.headers['x-real-ip'] || req.connection.remoteAddress // Log ip address.

  // Check if there are suspicious characters in request payload
  if (!checkParams(req.body) || !checkParams(req.params) || !checkParams(req.query) || !checkParams(req.cookies)) {
    console.log('Suspicious Payload In Route: ' + req.url);
    lib.bot.sendGrpMessage('*-WARNING- Suspicious Payload!*\n' + 'Route: ' + req.url);
    res.status(401).json({
      status: false,
      message: 'Suspicious Activity Detected'
    });
    return;
  }

  // Skip APIkey and Hmac check if is users endpoints
  const skipHmacUrls = [...lib.CONFIG.ENDPOINTS.PUBLIC_URLS, ...lib.CONFIG.ENDPOINTS.PRIVATE_URLS];

  if ((req.method === 'GET') && (originalUrl.includes('?'))) {
    originalUrl = originalUrl.substring(0, originalUrl.indexOf('?'));
  }

  await skipHmacUrls.forEach((url, index) => {
    if (url === originalUrl) {
      skip = true; // Not merchant API, so skip.
      next();
    }
  });

  if (!skip) {
    if (!apiKey || !hmac) {
      res.status(401).json({
        message: 'Permission denied: apikey or HMAC not found in header.'
      });
      return;
    }

    try {
      const keyObj = await lib.SQL.api_keys.findOne({
        where: {
          api_apiKey: apiKey
        }
      });

      if (keyObj == null) {
        res.status(401).json({
          status: false,
          message: 'Permission denied: Invalid API Key.'
        });
        return;
      }

      if (keyObj.api_isActive === false) {
        res.status(401).json({
          status: false,
          message: 'Permission denied: API Key no longer active.'
        });
        return;
      }

      var hmacGen = lib.createHmac('sha512', Buffer.from(keyObj.api_secretKey)).update(JSON.stringify(body)).digest('hex');

      if (hmacGen !== hmac) {
        // hmac does not matches SHA512(req.body)
        res.status(401).json({
          status: false,
          message: 'Permission denied: Invalid HMAC.'
        });
        return;
      } else {
        // Insert owner of API Key into req body for other functions to use
        req.body.userId = keyObj.api_userId;
        next();

        // ------------------------------------------------------
        // SAMPLE CODE FOR ONLY ALLOWING WHITELISTED IPs
        // ------------------------------------------------------

        // let arrWhitelistedIps = await lib.SQL.whitelisted_ips.findAll({
        //     where: {
        //         wip_apiKey: apiKey
        //     }
        // })

        // // No Whitelisted IP set. Allow Request
        // if(arrWhitelistedIps.length == 0){
        //     next();
        //     return;
        // }

        // let epoch = Math.round((new Date()).getTime() / 1000);

        // for(let whitelistData of arrWhitelistedIps){
        //     if(whitelistData.wip_ip == ipAddr){
        //         whitelistData.update({wip_lastAccess: epoch})
        //         next();
        //         return;
        //     }
        // }

        // console.log('Attempted API Key use from unauthorised IP');
        // res.status(401).json({
        //     status: false,
        //     message: 'Permission denied'
        // });
        // return;

        // ------------------------------------------------------
      }
    } catch (err) {
      console.log(err);
      const params = {
        type: 'Router',
        cat1: 'Router',
        msg: 'Failed in Router',
        error: err
      };

      lib.logger.LogError(params);

      res.status(400).json({
        success: false,
        message: 'Something went wrong, please try again later.'
      });
    }
  } // end of skip check
});

// Combines public urls and API Key only urls
const publicEndpoints = [...lib.CONFIG.ENDPOINTS.PUBLIC_URLS, ...lib.CONFIG.ENDPOINTS.APIKEY_URLS];

router.use(lib.expressJWT({
  secret: lib.CONFIG.JWT.SECRET,
  algorithms: ['HS256']
}).unless({
  // For External API Requests
  path: publicEndpoints
}));

router.use(function (err, req, res, next) {
  if (err) {
    res.status(401).send({
      status: false,
      message: 'JWT token expired or no authorisation token was found. Error: 338'
    });
  } else {
    next();
  }
});

// ------------------------------------------------------
// SAMPLE CODE FOR BLOCKING CERTAIN URLS
// ------------------------------------------------------

// Run Functions before processing API
router.use(async function (req, res, next) {
  // let kycUrls = lib.CONFIG.ENDPOINTS.KYC_PATHS;

  // if(kycUrls.includes(req.path)){

  //     if(req.user != undefined){

  //         let user = await lib.SQL.users.findOne({
  //             where: {
  //                 id: req.user.id,
  //                 isActive: true
  //             }
  //         })

  //         if(user.kycStatus != 'APPROVED'){
  //             res.status(401).send({
  //                 status: false,
  //                 message: "User verification has not been completed or user is inactive"
  //             });
  //         }else{
  //             next();
  //         }
  //         return;
  //     }

  // }else{
  next();
  // }
});
// ------------------------------------------------------

const createAccountLimiter = lib.rateLimit({
  windowMs: 1 * 60 * 1000, // 1 mins window
  max: 100, // start blocking after 5 requests
  message: {
    status: false,
    message: 'Rate limit reached, do not abuse the service. Please try again in 5 minutes.'
  }
});

function checkParams (param) {
  // Whitelisted Characters Allowed in Params
  const regex = /^[A-Za-z0-9 /,@&_:.%$#*=-]*$/;

  for (const index in param) {
    let val = param[index];

    if (utility.isValidJSONString(val)) {
      val = JSON.parse(val);
    }

    if (val && typeof val === 'object' && val.constructor === Object) {
      if (!checkParams(val)) {
        return false;
      }
    } else if (!regex.test(val)) {
      console.log('Param Value Rejected: ' + val);
      lib.bot.sendGrpMessage('*-WARNING- Parameter Value Rejected!*\n' + 'Params: ' + JSON.stringify(param) + '\n' + 'Rejected Value: ' + val);
      return false;
    }
  }

  return true;
};

// =================================================
// TEST ROUTES
// =================================================
router.route('/test/ipn_listener')
  .post(createAccountLimiter, function (req, res) {
    testIPN.retrieveVerifyDataIPN(req, res);
  });

// =================================================
// Users ROUTES
// =================================================

router.route('/users/login')
  .post(createAccountLimiter, function (req, res) {
    users.loginUser(req, res);
  });

router.route('/users/verify')
  .get(createAccountLimiter, function (req, res) {
    users.verifyUser(req, res);
  });

router.route('/users')
  .post(createAccountLimiter, function (req, res) {
    users.registerUser(req, res);
  });

router.route('/users')
  .get(createAccountLimiter, function (req, res) {
    users.getUserInfo(req, res);
  });

router.route('/users')
  .put(createAccountLimiter, function (req, res) {
    users.updateUserInfo(req, res);
  });

// =================================================
// Wallet Routes
// =================================================

/* GET Get Market Price of ETH/BTC */
/**
 * @swagger
 *
 * /wallet/market/:
 *   get:
 *     description: Get Market Price
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: node
 *         description: What type of node to create the address in eg. Bitcoin blockchain or Ethereum Blockchain
 *         in: query
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Result
 */
router.route('/wallet/market')
  .get(createAccountLimiter, function (req, res) {
    wallet.getFeePrice(req, res);
  });

router.route('/wallet/balances')
  .get(createAccountLimiter, function (req, res) {
    wallet.getBalance(req, res);
  });

router.route('/wallet/addresses')
  .get(createAccountLimiter, function (req, res) {
    wallet.getWalletAddresses(req, res);
  })
/* POST Create Blockchain Address */
/**
 * @swagger
 *
 * /wallet/addresses/:
 *   post:
 *     description: Create Blockchain Address
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: node
 *         description: What type of node to create the address in eg. Bitcoin blockchain or Ethereum Blockchain
 *         in: body
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Result
 */
  .post(createAccountLimiter, function (req, res) {
    wallet.createAddr(req, res);
  });

router.route('/wallet/addresses/validate')
/* GET Validate Blockchain Address */
/**
 * @swagger
 *
 * /wallet/addresses/validate:
 *   get:
 *     description: Validate Blockchain Address
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: node
 *         description: What type of node is the address from eg. Bitcoin blockchain or Ethereum Blockchain
 *         in: query
 *         required: true
 *         type: string
 *       - name: address
 *         description: Blockchain Address
 *         in: query
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Result
 */
  .get(createAccountLimiter, function (req, res) {
    wallet.validateAddr(req, res);
  });

router.route('/wallet/keys')
  .get(createAccountLimiter, function (req, res) {
    wallet.getPrivateKey(req, res);
  })
  .post(createAccountLimiter, function (req, res) {
    wallet.importPrivateKey(req, res);
  });

/* GET Get Transaction */
/**
 * @swagger
 *
 * /wallet/transactions/:
 *   get:
 *     description: Validate Blockchain Address
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: node
 *         description: What type of node is the address from eg. Bitcoin blockchain or Ethereum Blockchain
 *         in: query
 *         required: true
 *         type: string
 *       - name: token
 *         description: Blockchain token
 *         in: query
 *         required: true
 *         type: string
 *       - name: txid
 *         description: Transaction ID
 *         in: query
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Result
 */
router.route('/wallet/transactions')
  .get(createAccountLimiter, function (req, res) {
    wallet.getTransaction(req, res);
  });

/* GET Estimate Network Fee */
/**
 * @swagger
 *
 * /wallet/estimateEthCost/:
 *   get:
 *     description: Estimate network fee
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: node
 *         description: What type of node is the address from eg. Bitcoin blockchain or Ethereum Blockchain
 *         in: query
 *         required: true
 *         type: string
 *       - name: token
 *         description: Blockchain token
 *         in: query
 *         required: true
 *         type: string
 *       - name: recipientAddr
 *         description: Recipient Wallet Address
 *         in: query
 *         required: true
 *         type: string
 *       - name: senderAddr
 *         description: Sender Wallet Address
 *         in: query
 *         required: true
 *         type: string
 *       - name: amount
 *         description: Amount to be sent
 *         in: query
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Result
 */
router.route('/wallet/estimateEthCost')
  .get(createAccountLimiter, function (req, res) {
    wallet.estimateEthCost(req, res);
  });

router.route('/wallet/send')
  .post(createAccountLimiter, function (req, res) {
    wallet.sendTransaction(req, res);
  });

router.route('/wallet/send/funded')
  .post(createAccountLimiter, function (req, res) {
    wallet.fundedSendTransaction(req, res);
  });

router.route('/wallet/transactions/pending')
  .get(createAccountLimiter, function (req, res) {
    wallet.getPendingTx(req, res);
  });

// =================================================
// Watch Address Routes
// =================================================

router.route('/watch')
  .post(createAccountLimiter, function (req, res) {
    watchAddress.addWatchAddr(req, res);
  })
  .put(createAccountLimiter, function (req, res) {
    watchAddress.updateWatchAddr(req, res);
  });

// export the router
module.exports = router;
