/* eslint-disable no-prototype-builtins */
const lib = require('./../lib.js');
const utility = require('./../utility.js');

// 1. Create Wallet Address
// 2. Send Transaction
// 3. Get Balance
// 4. Get Transaction Info
// 5. Get Transaction History
// 6. Get Pending Transactions (BTC)
// 7. Get Private Key

async function createAddr (req, res) {
  try {
    const node = req.body.node;

    // const userId = req.body.userId; // Automatically added by router.js
    // const watch = req.body.watch || true;

    // check if required params are passed in
    // if (!node || !userId) {
    if (!node) {
      res.status(400).json({
        success: false,
        message: 'Incorrect or missing parameters, please refer to API doc.'
      });
      return;
    }

    const token = 'USDT';
    const nodeToken = node + '_' + token;

    const params = {
      coinType: nodeToken
    };

    const newAddr = await lib.walletObj.createAcc(params);

    // await lib.SQL.ethereum_keys.update({ ek_userId: userId }, { where: { ek_address: newAddr.address } });

    // if (watch) {
    //   lib.SQL.watch_addresses.create({
    //     wa_address: newAddr.address,
    //     wa_userId: userId,
    //     wa_node: node,
    //     wa_status: 'WATCHING'
    //   });
    // }

    res.status(200).json({
      success: true,
      address: newAddr.address,
      privateKey: newAddr.privateKey
    });
  } catch (err) {
    const params = {
      type: 'API',
      cat1: 'createAddr',
      msg: 'Failed in API Call',
      error: err
    };

    lib.logger.LogError(params);

    res.status(500).json({
      success: false,
      message: 'Something went wrong, please try again later.'
    });
  }
}

async function validateAddr (req, res) {
  try {
    const getParams = (req.query.hasOwnProperty('params')) ? JSON.parse(req.query.params) : {};

    const node = getParams.node;
    const address = getParams.address;

    // check if required params are passed in
    if (!node || !address) {
      res.status(400).json({
        success: false,
        message: 'Incorrect or missing parameters, please refer to API doc.'
      });
      return;
    }

    const token = 'USDT';
    const nodeToken = node + '_' + token;

    const valParams = {
      coinType: nodeToken,
      address: address
    };

    const valid = await lib.walletObj.validateAddr(valParams);

    res.status(200).json({
      success: true,
      valid: valid
    });
  } catch (err) {
    const params = {
      type: 'API',
      cat1: 'validateAddr',
      msg: 'Failed in API Call',
      error: err
    };

    lib.logger.LogError(params);

    res.status(500).json({
      success: false,
      message: 'Something went wrong, please try again later.'
    });
  }
}

async function getWalletAddresses (req, res) {
  try {
    const node = req.query.node;
    const userId = req.body.userId;

    // check if required params are passed in
    if (!node) {
      res.status(400).json({
        success: false,
        message: 'Incorrect or missing parameters, please refer to API doc.'
      });
      return;
    }

    const nodeToken = node + '_' + node;

    const params = {
      coinType: nodeToken,
      userId: userId
    };

    const accounts = await lib.walletObj.listAccounts(params);

    res.status(200).json({
      success: true,
      addresses: accounts
    });
  } catch (err) {
    const params = {
      type: 'API',
      cat1: 'getWalletAddresses',
      msg: 'Failed in API Call',
      error: err
    };

    lib.logger.LogError(params);

    res.status(500).json({
      success: false,
      message: 'Something went wrong, please try again later.'
    });
  }
}

async function getPrivateKey (req, res) {
  try {
    const getParams = (req.query.hasOwnProperty('params')) ? JSON.parse(req.query.params) : {};

    const node = getParams.node;
    const address = getParams.address;

    // check if required params are passed in
    if (!node || !address) {
      res.status(400).json({
        success: false,
        message: 'Incorrect or missing parameters, please refer to API doc.'
      });
      return;
    }

    const token = 'USDT';
    const nodeToken = node + '_' + token;

    const params = {
      coinType: nodeToken,
      address: address
    };

    const account = await lib.walletObj.getPrivateKey(params);

    res.status(200).json({
      success: true,
      privateKey: account.privateKey
    });
  } catch (err) {
    const params = {
      type: 'API',
      cat1: 'getPrivateKey',
      msg: 'Failed in API Call',
      error: err
    };

    lib.logger.LogError(params);

    res.status(500).json({
      success: false,
      message: 'Something went wrong, please try again later.'
    });
  }
}

async function importPrivateKey (req, res) {
  try {
    const node = req.body.node;
    const address = req.body.address;
    let privateKey = req.body.privateKey;

    // check if required params are passed in
    if (!node || !address || !privateKey) {
      res.status(400).json({
        success: false,
        message: 'Incorrect or missing parameters, please refer to API doc.'
      });
      return;
    }

    const token = 'USDT';
    const nodeToken = node + '_' + token;

    const params = {
      coinType: nodeToken,
      address: address,
      privateKey: privateKey
    };

    privateKey = await lib.walletObj.importPrivateKey(params);

    res.status(200).json({
      success: true,
      privateKey: privateKey
    });
  } catch (err) {
    const params = {
      type: 'API',
      cat1: 'importPrivateKey',
      msg: 'Failed in API Call',
      error: err
    };

    lib.logger.LogError(params);

    res.status(500).json({
      success: false,
      message: 'Something went wrong, please try again later.'
    });
  }
}

async function getFeePrice (req, res) {
  try {
    // const getParams = (req.query.hasOwnProperty('params')) ? JSON.parse(req.query.params) : {};

    const node = req.query.node;

    // check if required params are passed in
    if (!node) {
      res.status(400).json({
        success: false,
        message: 'Incorrect or missing parameters, please refer to API doc.'
      });
      return;
    }

    const nodeToken = node + '_' + 'ETH';

    const params = {
      coinType: nodeToken
    };

    const feePrice = await lib.walletObj.getFeePrice(params);

    res.status(200).json({
      success: true,
      feePrice: feePrice,
      currency: 'USD'
    });
  } catch (err) {
    const params = {
      type: 'API',
      cat1: 'getBalance',
      msg: 'Failed in API Call',
      error: err
    };

    lib.logger.LogError(params);

    res.status(500).json({
      success: false,
      message: 'Something went wrong, please try again later.'
    });
  }
}

async function getBalance (req, res) {
  try {
    const getParams = (req.query.hasOwnProperty('params')) ? JSON.parse(req.query.params) : {};

    const node = getParams.node;
    const token = getParams.token;
    const address = getParams.address;

    // check if required params are passed in
    if (!node || !token || !address) {
      res.status(400).json({
        success: false,
        message: 'Incorrect or missing parameters, please refer to API doc.'
      });
      return;
    }

    const nodeToken = node + '_' + token;

    const params = {
      coinType: nodeToken,
      address: address
    };

    const balance = await lib.walletObj.getBalance(params);

    res.status(200).json({
      success: true,
      balance: balance
    });
  } catch (err) {
    const params = {
      type: 'API',
      cat1: 'getBalance',
      msg: 'Failed in API Call',
      error: err
    };

    lib.logger.LogError(params);

    res.status(500).json({
      success: false,
      message: 'Something went wrong, please try again later.'
    });
  }
}

async function getTransaction (req, res) {
  try {
    // const getParams = (req.query.hasOwnProperty('params')) ? JSON.parse(req.query.params) : {};

    const node = req.query.node;
    const token = req.query.token;
    const txid = req.query.txid;

    // check if required params are passed in
    if (!node || !token || !txid) {
      res.status(400).json({
        success: false,
        message: 'Incorrect or missing parameters, please refer to API doc.'
      });
      return;
    }

    const nodeToken = node + '_' + token;

    const params = {
      coinType: nodeToken,
      txid: txid
    };

    const transaction = await lib.walletObj.getTransaction(params);

    res.status(200).json({
      success: true,
      transaction: transaction
    });
  } catch (err) {
    const params = {
      type: 'API',
      cat1: 'getTransaction',
      msg: 'Failed in API Call',
      error: err
    };

    lib.logger.LogError(params);

    res.status(500).json({
      success: false,
      message: 'Something went wrong, please try again later.'
    });
  }
}

async function getPendingTx (req, res) {
  try {
    const getParams = (req.query.hasOwnProperty('params')) ? JSON.parse(req.query.params) : {};

    const orderIds = getParams.orderIds;

    // check if required params are passed in
    if (!orderIds || !Array.isArray(orderIds)) {
      res.status(400).json({
        success: false,
        message: 'Incorrect or missing parameters, please refer to API doc.'
      });
      return;
    }

    // Pending transactions function is only allowed for BTC Blockchain
    const orders = await lib.sequelize.usdt_tx_batches.findAll({ where: { node: 'BTC', orderId: orderIds } });

    const pendingTx = {};

    for (const orderObj of orders) {
      const node = orderObj.node;
      const token = orderObj.token;
      const nodeToken = node + '_' + token;

      const params = {
        coinType: nodeToken,
        address: orderObj.paymentAddr
      };

      const result = await lib.walletObj.getPendingTx(params);

      pendingTx[orderObj.orderId] = result;

      await utility.delay(2);
    }

    res.status(200).json({
      success: true,
      pendingTx: pendingTx
    });
  } catch (err) {
    const params = {
      type: 'API',
      cat1: 'getPendingTx',
      msg: 'Failed in API Call',
      error: err
    };

    lib.logger.LogError(params);

    res.status(500).json({
      success: false,
      message: 'Something went wrong, please try again later.'
    });
  }
}

async function estimateEthCost (req, res) {
  try {
    const node = req.query.node;
    const token = req.query.token;
    const recipientAddr = req.query.recipientAddr;
    const senderAddr = req.query.senderAddr;
    const amount = req.query.amount;

    // check if required params are passed in
    if (!node || !token || !recipientAddr || !senderAddr || !amount) {
      res.status(400).json({
        success: false,
        message: 'Incorrect or missing parameters, please refer to API doc.'
      });
      return;
    }

    const nodeToken = node + '_' + token;

    const params = {
      coinType: nodeToken,
      sender: senderAddr,
      recipient: recipientAddr,
      amount: amount
    };

    const cost = await lib.walletObj.estimateEtherCost(params);

    res.status(200).json({
      success: true,
      cost: cost
    });
  } catch (err) {
    const params = {
      type: 'API',
      cat1: 'estimateEthCost',
      msg: 'Failed in API Call',
      error: err
    };

    lib.logger.LogError(params);

    res.status(500).json({
      success: false,
      message: 'Something went wrong, please try again later.'
    });
  }
}

async function sendTransaction (req, res) {
  try {
    const node = req.body.node;
    const token = req.body.token;
    const recipientAddr = req.body.recipientAddr;
    const senderAddr = req.body.senderAddr;
    const amount = req.body.amount;
    const funderAddr = req.body.funderAddr;

    // check if required params are passed in
    if (!node || !token || !recipientAddr || !senderAddr || !amount || (node === 'BTC' && !funderAddr)) {
      res.status(400).json({
        success: false,
        message: 'Incorrect or missing parameters, please refer to API doc.'
      });
      return;
    }

    const nodeToken = node + '_' + token;

    const params = {
      coinType: nodeToken,
      sender: senderAddr,
      recipient: recipientAddr,
      amount: amount,
      funder: funderAddr
    };

    const txid = await lib.walletObj.sendTransaction(params);

    res.status(200).json({
      success: true,
      txid: txid
    });
  } catch (err) {
    const params = {
      type: 'API',
      cat1: 'sendTransaction',
      msg: 'Failed in API Call',
      error: err
    };

    lib.logger.LogError(params);

    res.status(500).json({
      success: false,
      message: err.message
    });
  }
}

async function fundedSendTransaction (req, res) {
  try {
    const node = req.body.node;
    const token = req.body.token;
    const recipientAddr = req.body.recipientAddr;
    const senderAddr = req.body.senderAddr;
    const amount = req.body.amount;
    const funderAddr = req.body.funderAddr;
    const userId = req.body.userId; // Automatically added by router.js

    // check if required params are passed in
    if (!node || !token || !recipientAddr || !senderAddr || !amount || !funderAddr) {
      res.status(400).json({
        success: false,
        message: 'Incorrect or missing parameters, please refer to API doc.'
      });
      return;
    }

    const nodeToken = node + '_' + token;

    // Check if we have private key for both sender and recipient
    const walletKeys = await lib.SQL.ethereum_keys.findAll({
      where: {
        ek_userId: userId,
        ek_address: [senderAddr, funderAddr]
      }
    });

    if (walletKeys.length < 2) {
      res.status(400).json({
        success: false,
        message: 'This account does not have access to either the sender or funder private key'
      });
      return;
    }

    // Check if sender has enough tokens
    // Get Sender Token Balance
    const getSenderTokenBalParams = {
      coinType: nodeToken,
      address: senderAddr
    };

    const senderTokenBal = await lib.walletObj.getBalance(getSenderTokenBalParams);
    // Extra to take into account gas cost to send from funder to sender
    if (senderTokenBal < amount) {
      res.status(400).json({
        success: false,
        message: 'Sender has insufficient token balance'
      });
      return;
    }

    // Estimate ether cost of transaction
    const estEthCost = await lib.walletObj.estimateEtherCost({
      coinType: nodeToken,
      sender: senderAddr,
      recipient: recipientAddr,
      amount: amount
    });

    // Get Sender Ethereum Balance
    const getSenderEthBalParams = {
      coinType: 'ETH_ETH',
      address: senderAddr
    };

    const senderEthBal = await lib.walletObj.getBalance(getSenderEthBalParams);

    // Send slightly extra just in case
    const ethToSend = parseFloat((estEthCost * 1.5) - senderEthBal);

    // If sender already has enough ETH for gas
    if (ethToSend <= 0) {
      const params = {
        coinType: nodeToken,
        sender: senderAddr,
        recipient: recipientAddr,
        amount: amount
      };

      const txid = await lib.walletObj.sendTransaction(params);

      res.status(200).json({
        success: true,
        txid: txid,
        message: 'Sender address already has sufficient funds. Transaction sent'
      });
      return;
    }

    // Check if funder has enough ETH for gas
    // Get Funder Ethereum Balance
    const getFunderEthBalParams = {
      coinType: 'ETH_ETH',
      address: funderAddr
    };

    const funderEthBal = await lib.walletObj.getBalance(getFunderEthBalParams);
    // Extra to take into account gas cost to send from funder to sender
    if (funderEthBal < (ethToSend * 1.1)) {
      res.status(400).json({
        success: false,
        message: 'Funder has insufficient ETH to fund gas'
      });
      return;
    }

    const params = {
      coinType: 'ETH_ETH',
      sender: funderAddr,
      recipient: senderAddr,
      amount: ethToSend.toFixed(8)
    };

    const txid = await lib.walletObj.sendTransaction(params);

    await lib.SQL.transactions.create({
      tx_node: node,
      tx_token: token,
      tx_funderTxId: txid,
      tx_status: 'OPEN',
      tx_amount: amount,
      tx_senderAddr: senderAddr,
      tx_recipientAddr: recipientAddr,
      tx_funderAddr: funderAddr
    });

    await lib.SQL.eth_fund_txs.create({
      eft_userId: userId,
      eft_type: 'FUNDED_SEND',
      eft_token: token,
      eft_amount: ethToSend.toFixed(8),
      eft_paymentAddr: senderAddr,
      eft_status: 'PENDING',
      eft_txid: txid
    });

    res.status(200).json({
      success: true
    });
  } catch (err) {
    const params = {
      type: 'API',
      cat1: 'fundedSendTransaction',
      msg: 'Failed in API Call',
      error: err
    };

    lib.logger.LogError(params);

    res.status(500).json({
      success: false,
      message: 'Something went wrong, please try again later.'
    });
  }
}

async function importTransaction (req, res) {
  try {
    const txid = req.body.txid;
    const node = req.body.node;

    // check if required params are passed in
    if (!txid || !node) {
      res.status(400).json({
        success: false,
        message: 'Incorrect or missing parameters, please refer to API doc.'
      });
      return;
    }

    const exists = await lib.SQL.transactions.findOne({
      where: {
        tx_transactionId: txid
      }
    });

    if (exists) {
      res.status(400).json({
        success: false,
        message: 'Transaction already exists in database'
      });
      return;
    }

    let params = {
      node: node,
      txid: txid
    };

    const token = await lib.walletObj.getTokenType(params);

    if (token === false) {
      res.status(400).json({
        success: false,
        message: 'Unsupported token detected.'
      });
      return;
    }

    const nodeToken = node + '_' + token;

    params = {
      coinType: nodeToken,
      txid: txid
    };

    const tx = await lib.walletObj.getTransaction(params);

    const upsertObj = {
      tx_node: node,
      tx_token: token,
      tx_transactionId: tx.txid || tx.hash,
      tx_status: 'PENDING',
      tx_amount: tx.amount,
      tx_senderAddr: tx.sendingaddress || tx.from,
      tx_recipientAddr: tx.referenceaddress || tx.to || tx.paymentAddr
    };

    if (node === 'ETH') {
      upsertObj.tx_ethGasPrice = tx.gasPrice;
      upsertObj.tx_ethNonce = tx.nonce;
    }

    if (tx.confirmations >= 1) {
      let feePrice = await lib.SQL.configs.findOne({ where: { conf_cat1: 'PRICE', conf_cat2: node, conf_cat3: 'USD' } });
      feePrice = feePrice.conf_value;
      const networkFeeUsd = feePrice * Number(tx.fee);

      params = {
        coinType: nodeToken,
        blockHash: tx.blockHash
      };

      const blockInfo = await lib.walletObj.getBlockInfo(params);
      const confirmedAt = blockInfo.time || blockInfo.timestamp;

      upsertObj.tx_status = 'SUCCESS';
      upsertObj.tx_networkFee = tx.fee;
      upsertObj.tx_networkFeeUsd = networkFeeUsd;
      upsertObj.tx_confirmedAt = new Date(Number(confirmedAt) * 1000) || new Date();

      if (node === 'ETH') {
        upsertObj.tx_ethGasUsed = tx.gasUsed;
      }
    }

    await lib.SQL.transactions.upsert(upsertObj);

    res.status(200).json({
      success: true,
      txid: txid
    });
  } catch (err) {
    const params = {
      type: 'API',
      cat1: 'importTransaction',
      msg: 'Failed in API Call',
      error: err
    };

    lib.logger.LogError(params);

    res.status(500).json({
      success: false,
      message: 'Something went wrong, please try again later.'
    });
  }
}

async function userCallbackUrl (txId) {
  try {
    const txObj = await lib.SQL.transactions.findOne({
      where: {
        tx_transactionId: txId
      },
      raw: true
    });

    const watchedAddr = await lib.SQL.watch_addresses.findOne({
      where: {
        wa_address: [txObj.tx_senderAddr, txObj.tx_recipientAddr]
      }
    });

    const user = watchedAddr.wa_userId;

    const userObj = await lib.SQL.users.findOne({
      where: {
        u_userId: user
      }
    });

    const callbackUrl = userObj.u_callbackUrl;

    if (callbackUrl == null) {
      return;
    }

    const options = {
      method: 'POST',
      uri: callbackUrl,
      headers: {
        'Content-type': 'application/json',
        Accept: 'application/json',
        'Accept-Charset': 'utf-8'
      },
      body: {
        transaction: txObj
      },
      json: true // Automatically parses the JSON string in the response
    };

    lib.requestpn(options).then(result => {
      return result;
    }).catch(err => {
      throw err;
    });
  } catch (err) {
    const params = {
      type: 'API',
      cat1: 'userCallbackUrl',
      msg: 'Failed in user callback',
      error: err
    };

    lib.logger.LogError(params);

    // res.status(500).json({
    //   success: false,
    //   message: 'Something went wrong, please try again later.'
    // })
  }
}

// --------------------------------------------------------------

module.exports = {

  createAddr,
  validateAddr,
  getWalletAddresses,
  getPrivateKey,
  importPrivateKey,
  getFeePrice,
  getBalance,
  getTransaction,
  getPendingTx,
  estimateEthCost,
  sendTransaction,
  fundedSendTransaction,
  importTransaction,
  userCallbackUrl

}; // end of module.exports
