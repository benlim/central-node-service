const lib = require('./../lib.js');
// const utility = require('./../utility.js')

// 1. Create Wallet Address
// 2. Send Transaction
// 3. Get Balance
// 4. Get Transaction Info
// 5. Get Transaction History
// 6. Get Pending Transactions (BTC)
// 7. Get Private Key

async function addWatchAddr (req, res) {
  try {
    const userId = req.body.userId;

    const node = req.body.node;
    const address = req.body.address;

    // check if required params are passed in
    if (!node || !address) {
      res.status(400).json({
        success: false,
        message: 'Incorrect or missing parameters, please refer to API doc.'
      });
      return;
    }

    const params = {
      coinType: node + '_' + node,
      address: address
    };
    const valid = await lib.walletObj.validateAddr(params);

    if (!valid) {
      res.status(400).json({
        success: false,
        message: node + ' address ' + address + ' is invalid.'
      });
      return;
    }

    const exists = await lib.SQL.watch_addresses.findOne({
      where: {
        wa_address: address
      }
    });

    if (exists) {
      res.status(400).json({
        success: false,
        message: 'Address ' + address + ' is already in watch list.'
      });
      return;
    }

    await lib.SQL.watch_addresses.create({
      wa_address: address,
      wa_userId: userId,
      wa_node: node,
      wa_status: 'WATCHING'
    });

    res.status(200).json({
      success: true,
      message: 'Successfuly added address to watch list'
    });
  } catch (err) {
    const params = {
      type: 'API',
      cat1: 'addWatchAddr',
      msg: 'Failed in API Call',
      error: err
    };

    lib.logger.LogError(params);

    res.status(500).json({
      success: false,
      message: 'Something went wrong, please try again later.'
    });
  }
}

async function updateWatchAddr (req, res) {
  try {
    const address = req.body.address;
    const status = req.body.status;

    // check if required params are passed in
    if (!address || !status) {
      res.status(400).json({
        success: false,
        message: 'Incorrect or missing parameters, please refer to API doc.'
      });
      return;
    }

    const watchAddr = await lib.SQL.watch_addresses.findOne({
      where: {
        wa_address: address
      }
    });

    if (watchAddr == null) {
      res.status(400).json({
        success: false,
        message: 'The address given does not exist'
      });
    }

    await watchAddr.update({
      wa_status: status
    });

    res.status(200).json({
      success: true,
      message: 'Successfuly updated watch address'
    });
  } catch (err) {
    const params = {
      type: 'API',
      cat1: 'updateWatchAddr',
      msg: 'Failed in API Call',
      error: err
    };

    lib.logger.LogError(params);

    res.status(500).json({
      success: false,
      message: 'Something went wrong, please try again later.'
    });
  }
}

// --------------------------------------------------------------

module.exports = {

  addWatchAddr,
  updateWatchAddr

}; // end of module.exports
