const lib = require('./../lib.js');
// const log = require('./../console_log.js')
const utility = require('./../utility.js');
// const aws = require('./../aws.js')
const SQL = lib.SQL;

async function sendVeriLink (userObj) {
  try {
    const vericode = lib.randomize('Aa0', 48) + '-' + userObj.email;
    var veriLink = lib.CONFIG.DOMAIN + lib.CONFIG.PREFIX + '/users/verify?v=' + vericode;

    await lib.nodemailer.createTestAccount(async (err1, account) => {
      const transporter = lib.nodemailer.createTransport({
        host: lib.CONFIG.EMAIL_SERVER.HOST,
        port: lib.CONFIG.EMAIL_SERVER.PORT,
        secure: true, // upgrade later with STARTTLS
        auth: {
          user: lib.CONFIG.EMAIL_SERVER.ADDRESS,
          pass: lib.CONFIG.EMAIL_SERVER.PASSWORD
        },
        tls: {
          rejectUnauthorized: false
        }
      });

      if (err1) {
        throw new Error(err1);
      } // end of if err

      // set mail options
      const mailOptions = {
        from: lib.CONFIG.EMAIL_SERVER.FROM,
        to: userObj.email,
        subject: 'Template Email Verification',
        text: 'Please verify your email address by clicking on the following link: ' + veriLink
      };

      await transporter.sendMail(mailOptions, async function (err2, info) {
        if (err2) {
          throw new Error(err2);
        } // end of if err

        // Update Database with latest verification code.
        await userObj.update({
          vericode: vericode,
          vericodeEpoch: Math.floor(new Date() / 1000)
        });
        return true;
      }); // end of await transporter.sendMail
    });
  } catch (err) {
    const params = {
      type: 'API',
      cat1: 'sendVeriLink',
      msg: 'Failed in API Call',
      error: err
    };

    lib.logger.LogError(params);

    // res.status(400).json({
    //   success: false,
    //   message: 'Something went wrong, please try again later.'
    // })
  }
} // end of sendVeriLink

async function registerUser (req, res) {
  try {
    var name = req.body.name || null;
    var email = req.body.email || null;
    var password = req.body.password || null;
    var mobileNum = req.body.mobileNum || null;

    if (!name || !email || !password || !mobileNum || (typeof password !== 'string') || (typeof name !== 'string') || (typeof email !== 'string')) {
      res.status(400).json({
        success: false,
        message: 'Incorrect or missing parameters, please refer to API doc.'
      });
      return;
    }

    const exists = await SQL.users.findOne({
      where: {
        u_email: email
      }
    });

    if (exists != null) {
      // user already exist, return
      res.status(400).json({
        success: false,
        message: 'Account already exists.'
      });
      return;
    }

    // User does not exist, register user!
    // encrypt password first
    lib.bcrypt.hash(password, lib.CONFIG.SALT_ROUNDS, async function (err, hash) {
      if (err) {
        throw new Error(err);
      } // end of if err

      const userId = await lib.uniqid.time();

      await SQL.users.create({
        u_userId: userId,
        u_name: name,
        u_email: email,
        u_password: hash,
        u_mobileNum: mobileNum
      });

      // const newUser = SQL.users.findOne({
      //   where: {
      //     u_userId: userId
      //   }
      // });

      // Send verification link to new user email
      // sendVeriLink(newUser);

      res.status(200).json({
        success: true,
        email: email,
        message: 'Successfully created account, please verify your email.'
      });
    }); // end of bcrypt
  } catch (err) {
    const params = {
      type: 'API',
      cat1: 'registerUser',
      msg: 'Failed in API Call',
      error: err
    };

    lib.logger.LogError(params);

    res.status(400).json({
      success: false,
      message: 'Something went wrong, please try again later.'
    });
  }
} // end of registerUser

async function verifyUser (req, res) {
  try {
    var url = new URL(lib.CONFIG.DOMAIN + req.url) || null;
    var code = url.searchParams.get('v') || null;
    var vericode = []; // declare empty array

    if (!code) {
      res.status(400).json({
        success: false,
        message: 'Something went wrong.'
      });
      return;
    }

    vericode = code.split('-');

    if (vericode[1]) {
      var email = vericode[1];
      // find user and verify code.

      const user = await SQL.users.findOne({
        where: {
          u_email: email
        }
      });

      if (user == null) {
        res.status(400).json({
          success: false,
          message: 'User does not exist.'
        });
        return;
      } else {
        if (user.u_vericode === code) {
          // Success!
          user.update({
            u_vericode: null,
            u_vericodeEpoch: null
          });

          res.writeHead(301, {
            Location: lib.CONFIG.MAIN + lib.CONFIG.STATUS_DIR + 'success.html'
          });
          res.end();

          // Update DB
          user.update({
            u_isVerified: true,
            u_vericode: null,
            u_vericodeEpoch: null
          });
        } else if (user.isVerified) {
          // Account already verified
          res.writeHead(301, {
            Location: lib.CONFIG.MAIN + lib.CONFIG.STATUS_DIR + 'verified.html'
          });
          res.end();
        } else {
          // Verification link is invalid or had expired.
          res.writeHead(301, {
            Location: lib.CONFIG.MAIN + lib.CONFIG.STATUS_DIR + 'failed.html'
          });
          res.end();
          return;
        }
      } // end of user verification
    } else {
      // Verification link is invalid or had expired.
      res.writeHead(301, {
        Location: lib.CONFIG.DOMAIN + lib.CONFIG.STATUS_DIR + 'failed.html'
      });
      res.end();
      return;
    } // end of else vericode
  } catch (err) {
    const params = {
      type: 'API',
      cat1: 'verifyUser',
      msg: 'Failed in API Call',
      error: err
    };

    lib.logger.LogError(params);

    res.status(400).json({
      success: false,
      message: 'Something went wrong, please try again later.'
    });
  }
} // end of verifyUser

async function loginUser (req, res) {
  try {
    var email = req.body.email || null;
    var password = req.body.password || null;

    if (!email || !password) {
      res.status(400).json({
        success: false,
        message: 'Incorrect or missing parameters, please refer to API doc.'
      });
      return;
    }

    const user = await SQL.users.findOne({
      where: {
        u_email: email,
        u_isActive: true
      }
    });

    if (!user) {
      res.status(400).json({
        success: false,
        message: 'Incorrect password or user email.'
      });
      return;
    }

    await lib.bcrypt.compare(password, user.u_password, async function (err, result) {
      if (result) {
        // password matched
        // generate json web token
        const token = await lib.jwt.sign({
          id: user.u_userId
        }, lib.CONFIG.ENDPOINTS.SECRET, {
          expiresIn: 900 // equivalent to 15 mins.
        });

        res.status(200).json({
          success: true,
          jwtToken: token
        });

        const ipAddr = req.headers['x-forwarded-for'] || req.headers['x-real-ip'] || req.connection.remoteAddress; // Log ip address.

        const apiKey = lib.CONFIG.IPDATA.APIKEY;
        const apiUrl = `https://api.ipdata.co/${ipAddr}?api-key=${apiKey}`;

        const options = {
          uri: apiUrl,
          headers: {
            'Content-type': 'application/json',
            Accept: 'application/json',
            'Accept-Charset': 'utf-8'
          },
          json: true // Automatically parses the JSON string in the response
        };

        lib.requestpn(options).then(result => {
          return result;
        }).catch(err => {
          // console.log(err);
          return err;
        }).then(response => {
          const location = (response.error != undefined) ? null : `Lat: ${response.latitude}, Long: ${response.longitude}, City: ${response.city}, Region: ${response.region}, Country: ${response.country_name}, Continent: ${response.continent_name} `;

          const date = new Date().toString();

          lib.SQL.user_activity_logs.create({
            ual_merchantId: user.merchantId,
            ual_category: 'LOG',
            ual_message: `Login IP: ${ipAddr} || Date: ${date} || Location: ${location || 'N/A'}`,
            ual_newVal: JSON.stringify(response)
          });

          lib.juice.juiceFile(__dirname + '/../../templates/login_logging.html', null, async (err, html) => {
            if (err) {
              console.log(err);
              return;
            }

            html = html.replace('ip_address_placeholder', ipAddr);
            html = html.replace('date_time_placeholder', date);
            html = html.replace('latitude_placeholder', response.latitude || 'N/A');
            html = html.replace('longitude_placeholder', response.longitude || 'N/A');
            html = html.replace('city_placeholder', response.city || 'N/A');
            html = html.replace('region_placeholder', response.region || 'N/A');
            html = html.replace('country_placeholder', response.country_name || 'N/A');
            html = html.replace('continent_placeholder', response.continent_name || 'N/A');

            // set mail options
            const mailOptions = {
              from: lib.CONFIG.EMAIL_SERVER.FROM,
              to: user.email,
              subject: 'Login Details',
              html: html,
              attachments: [{
                filename: 'bluepay_logo.png',
                path: __dirname + '/../../templates/images/bluepay_logo.png',
                cid: 'logo'
              }]
            };

            const transporter = lib.nodemailer.createTransport({
              host: lib.CONFIG.EMAIL_SERVER.HOST,
              port: lib.CONFIG.EMAIL_SERVER.PORT,
              secure: true, // upgrade later with STARTTLS
              auth: {
                user: lib.CONFIG.EMAIL_SERVER.ADDRESS,
                pass: lib.CONFIG.EMAIL_SERVER.PASSWORD
              },
              tls: {
                rejectUnauthorized: false
              }
            });

            transporter.sendMail(mailOptions, async function (error, info) {
              if (error) {
                console.log(error);
                throw error;
              }
            });
          });
        });
      } else {
        res.status(400).json({
          success: false,
          message: 'Incorrect password or user email.'
        });
      } // end of else
    }); // end of bcrypt compare
  } catch (err) {
    const params = {
      type: 'API',
      cat1: 'loginUser',
      msg: 'Failed in API Call',
      error: err
    };

    lib.logger.LogError(params);

    res.status(400).json({
      success: false,
      message: 'Something went wrong, please try again later.'
    });
  }
} // end of loginUser

async function getUserInfo (req, res) {
  try {
    const user = await SQL.users.findOne({
      where: {
        u_email: req.email,
        u_isActive: true
      }
    });

    if (user == null) {
      res.status(400).json({
        success: false,
        message: 'User does not exist'
      });
      return;
    }

    const authMethod = (user.u_otp_secret !== null) ? 'MOBILE' : 'EMAIL';

    res.status(200).json({
      success: true,
      isVerified: user.u_isVerified,
      isAdmin: user.u_isAdmin,
      kycStatus: user.u_kycStatus,
      registeredOn: user.u_createdAt,
      userId: user.u_userId,
      name: user.u_name,
      email: user.u_email,
      country: user.u_country,
      mobileNum: user.u_mobileNum,
      authMethod: authMethod
    });
  } catch (err) {
    const params = {
      type: 'API',
      cat1: 'loginUser',
      msg: 'Failed in API Call',
      error: err
    };

    lib.logger.LogError(params);

    res.status(400).json({
      success: false,
      message: 'Something went wrong, please try again later.'
    });
  }
}

async function updateUserInfo (req, res) {
  try {
    const user = await SQL.users.findOne({
      where: {
        u_email: req.email,
        u_isActive: true
      }
    });

    if (user == null) {
      res.status(400).json({
        success: false,
        message: 'User does not exist'
      });
      return;
    }

    await user.update({
      u_mobileNum: req.body.mobileNum
    });

    res.status(200).json({
      success: true,
      message: 'Successfully updated user data.'
    });
  } catch (err) {
    const params = {
      type: 'API',
      cat1: 'updateUserInfo',
      msg: 'Failed in API Call',
      error: err
    };

    lib.logger.LogError(params);

    res.status(400).json({
      success: false,
      message: 'Something went wrong, please try again later.'
    });
  }
}

async function generateApiKey (userId, label) {
  try {
    const exists = lib.SQL.api_keys.findOne({
      where: {
        api_label: label,
        api_userId: userId
      }
    });

    if (exists) {
      throw new Error('Label already exists');
    }

    var apiKey = utility.randomString(48);
    var secret = utility.randomString(48);

    await SQL.api_keys.create({
      api_userId: userId,
      api_label: label,
      api_apiKey: apiKey,
      api_secretKey: secret,
      api_createdAt: new Date()
    });

    return {
      success: true,
      keys: {
        key: apiKey,
        secret: secret
      },
      message: 'Successfully generated API key.'
    };
  } catch (err) {
    const params = {
      type: 'API',
      cat1: 'generateApiKey',
      msg: 'Failed in API Call',
      error: err
    };

    lib.logger.LogError(params);

    // res.status(400).json({
    //   success: false,
    //   message: 'Something went wrong, please try again later.'
    // })
  }
}

// --------------------------------------------------------------

module.exports = {

  sendVeriLink,
  registerUser,
  verifyUser,
  loginUser,
  getUserInfo,
  updateUserInfo,
  generateApiKey

}; // end of module.exports
