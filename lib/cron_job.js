/* eslint-disable no-async-promise-executor */

const lib = require('./lib.js');
const utility = require('./utility.js');
// const log = require('./console_log.js')
const CronJob = require('cron').CronJob;
const SQL = lib.SQL;

// Update estimated network fee at intervals of 10 seconds.
setInterval(async function () {
  updateTokenPrices();

  sendFundedTransactions();
}, 10 * 1000);

// Wait until DB connects before initiating
setTimeout(async () => {
  await utility.delay(2000);

  // Confirm currently pending transactions
  confirmPendingTx();

  // Subscribe to ETH_ETH transactions
  lib.walletObj.watchPendingTx({ coinType: 'ETH_ETH' });

  // Subscribe to ETH_USDT transactions
  lib.walletObj.watchPendingTx({ coinType: 'ETH_USDT' });
}, 1 * 1000);

async function updateTokenPrices () {
  try {
    const tokens = ['BTC', 'ETH'];

    for (const token of tokens) {
      const coinType = token + '_' + token;

      const tokenPrice = await lib.walletObj.getFeePrice({ coinType: coinType });

      await lib.SQL.configs.update({
        conf_value: tokenPrice,
        conf_updatedAt: new Date()
      }, {
        where: {
          conf_cat1: 'PRICE',
          conf_cat2: token,
          conf_cat3: 'USD'
        }
      });
    }
  } catch (err) {
    const params = {
      type: 'CRONJOB',
      cat1: 'updateTokenPrices',
      msg: 'Failed in Cron Job',
      error: err
    };

    lib.logger.LogError(params);
  }
}

async function confirmPendingTx () {
  try {
    const pendingTx = await lib.SQL.transactions.findAll({
      where: {
        tx_status: 'PENDING'
      }
    });

    let rateLimit = 0;

    for (const tx of pendingTx) {
      rateLimit++;

      if (rateLimit === 30) {
        await utility.delay(30 * 1000);
        rateLimit = 0;
      }

      lib.walletObj.arrCoinCores.ETH_ETH.confirmTransaction(tx);
    }
  } catch (err) {
    const params = {
      type: 'CRONJOB',
      cat1: 'confirmPending',
      msg: 'Failed in Cron Job',
      error: err
    };

    lib.logger.LogError(params);
  }
}

async function sendFundedTransactions () {
  return new Promise(async (resolve, reject) => {
    try {
      const fundingEthTxs = await SQL.eth_fund_txs.findAll({
        where: {
          eft_status: 'PENDING'
        }
      });

      if (fundingEthTxs.length < 1) {
        return resolve();
      }

      for (const fundData of fundingEthTxs) {
        const nodeToken = 'ETH_' + fundData.eft_token;

        // Get Ethereum Balance
        const getEthBalParams = {
          coinType: 'ETH_ETH',
          address: fundData.eft_paymentAddr
        };

        const ethBal = await lib.walletObj.getBalance(getEthBalParams);

        if (ethBal === 0) {
          continue;
        }

        const txParams = {
          coinType: 'ETH_ETH',
          txid: fundData.eft_txid
        };

        const fundTxData = await lib.walletObj.getTransaction(txParams);

        if (!fundTxData.valid) {
          continue;
        }

        await fundData.update({
          eft_status: 'SUCCESS',
          eft_sendFee: fundTxData.fee,
          eft_ethGasUsed: fundTxData.gasUsed,
          eft_ethGasPrice: fundTxData.gasPrice,
          eft_ethNonce: fundTxData.nonce,
          eft_confirmedAt: new Date()
        });

        const txToSend = await SQL.transactions.findOne({
          where: {
            tx_funderTxId: fundData.eft_txid
          }
        });

        const params = {
          coinType: nodeToken,
          sender: txToSend.tx_senderAddr,
          recipient: txToSend.tx_recipientAddr,
          amount: txToSend.tx_amount
        };

        const txid = await lib.walletObj.sendTransaction(params);

        await txToSend.update({
          tx_transactionId: txid,
          tx_status: 'PENDING'

        });
      } // end of fundingEthTxs.forEach
    } catch (err) {
      const params = {
        type: 'CRONJOB',
        cat1: 'sendFundedTransactions',
        msg: 'Failed in Cron Job',
        error: err
      };

      lib.logger.LogError(params);
      return reject(new Error());
    }

    return resolve();
  });
}

// async function update_estimated_network_fees() {

//     try{

//         let netFeeObj = await SQL.configs.findAll({
//             where: {
//                 conf_cat1: 'NETWORK_FEE'
//             }
//         });

//         for(let netFeeRow of netFeeObj){

//             let nodeToken = netFeeRow.conf_cat2 + '_' + netFeeRow.conf_cat3;
//             let size = 405;
//             let blocks = 6;

//             // Set default sender, recipient, amount for Eth Transaction estimation (Does not perform transaction)
//             let params = {
//                 coinType: nodeToken,
//                 size: size,
//                 blocks: blocks,
//                 sender: lib.CONFIG.ETH_WALLET_ADDR.WITHDRAW,
//                 recipient: lib.CONFIG.ETH_WALLET_ADDR.COLD,
//                 amount: '0'
//             }

//             let estimatedNetFee = await lib.walletObj.estimateFee(params);

//             await netFeeRow.update({
//                 conf_value: Number(estimatedNetFee).toFixed(4)
//             });
//         }

//     }catch(err){
//         console.log(err);
//         throw err;
//     }

// } //end of update_estimated_network_fees

// Declaration

// For Payment Gateway
// flags are to prevent cron from creating new promises before functions are complete
// let fund_eth_wallets_flag = true

// Schduled Functions at Intervals
setInterval(async function () {
  // updates sendFeeUsd in usdt_clear_batches table every 5 mins (CMC API ratelimit)
  // update_sendFeeUsd()
}, 3000);

setInterval(async function () {
  const now = new Date();
  now.setUTCHours(now.getUTCHours() + 8);
  await lib.bot.sendGrpMessage('*' + now.toUTCString().replace('GMT', 'UTC +8') + '*');
}, (60 * 60 * 1000));

// Scheduled Non Interval Functions
const schedule = new CronJob({
  cronTime: '* * * * * *',
  onTick: function () {

    // if (fund_eth_wallets_flag) {
    //     fund_eth_wallets_job()
    //         .then(() => {
    //             fund_eth_wallets_flag = true
    //                 //console.log("\x1b[33m%s\x1b[0m", '-I- Completed schedule_usdt_clear.')
    //         })
    //         .catch(e => {
    //             fund_eth_wallets_flag = true;
    //             log.error(e);
    //             let params = {
    //                 type: "Cron",
    //                 cat1: "-E[5]-",
    //                 msg: "Failed in fund_eth_wallets_job()",
    //                 error: e
    //             }
    //             ErrorHandler.LogError(params);
    //         })
    // }

  },
  timeZone: 'Asia/Kuala_Lumpur'
});

schedule.start();
