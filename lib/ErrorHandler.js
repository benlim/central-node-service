const lib = require('./lib');
const log = require('./console_log.js');

async function LogError (params) {
  var error = params.error;
  var shorten;
  var stack;

  console.log(error);
  if (error.stack) {
    stack = JSON.stringify(error.stack).toString();
    if (error.params !== undefined) {
      stack += '\nParams : ' + JSON.stringify(error.params).toString();
    }
    stack = stack.substring(1, stack.length);
    shorten = stack.split('\\n')[0];
  } else {
    shorten = stack = params.error;
  }
  var botmsg = 'Type : ' + params.type + '\n\nError Message : ' + params.msg + '\n\nMore : ' + shorten;
  lib.bot.sendGrpMessage(botmsg);

  await lib.SQL.logs.create({
    // values
    log_type: params.type,
    log_cat1: params.cat1,
    log_cat2: params.cat2,
    log_cat3: params.cat3,
    log_message: params.msg,
    log_error: stack
  }).catch(err => {
    console.log('\n\n\n');
    log.error(err);
    console.log('\n\n\n');
  });
}

module.exports = {
  LogError
};
