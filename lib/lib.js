// EXPORT
module.exports.CONFIG = require('../central-node.js');
module.exports.SQL = require('../db/sql.js');

module.exports.sequelize = require('sequelize');
module.exports.fs = require('fs');
module.exports.expressJWT = require('express-jwt');
module.exports.jwt = require('jsonwebtoken');
module.exports.nodemailer = require('nodemailer');
module.exports.bcrypt = require('bcrypt');
module.exports.rateLimit = require('express-rate-limit');
module.exports.juice = require('juice');
module.exports.qrcode = require('qrcode');
module.exports.crypto = require('crypto-js');
module.exports.awssdk = require('aws-sdk');
module.exports.web3 = require('web3');
module.exports.abiDecoder = require('abi-decoder');
module.exports.axios = require('axios');

// Import OTP Library and Configure Options
const otplib = require('otplib');
otplib.authenticator.options = {
  // Tokens in the previous and future x-windows that should be considered valid.
  // If integer, same value will be used for both.
  // Alternatively, define array: [previous, future]
  // Default = 0
  window: 0
};

module.exports.otp = otplib;

// Sequelize Operators
module.exports.Op = require('sequelize').Op;

// Web Socket
const WebSocketClass = require('./webSocket.js');
module.exports.webSocket = new WebSocketClass();

// Error Handler
module.exports.logger = require('./ErrorHandler.js');

// Source
module.exports.bot = require('./bot.js');

// Wallet Object
const WalletClass = require('./wallet/Wallet.class.js');
module.exports.walletObj = new WalletClass();
