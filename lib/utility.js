/* eslint-disable no-prototype-builtins */
const lib = require('./lib.js');
// const log = require('./console_log.js')
const SQL = lib.SQL;

const PADDING = [
  [16, 16, 16, 16,
    16, 16, 16, 16,
    16, 16, 16, 16,
    16, 16, 16, 16],

  [15, 15, 15, 15,
    15, 15, 15, 15,
    15, 15, 15, 15,
    15, 15, 15],

  [14, 14, 14, 14,
    14, 14, 14, 14,
    14, 14, 14, 14,
    14, 14],

  [13, 13, 13, 13,
    13, 13, 13, 13,
    13, 13, 13, 13,
    13],

  [12, 12, 12, 12,
    12, 12, 12, 12,
    12, 12, 12, 12],

  [11, 11, 11, 11,
    11, 11, 11, 11,
    11, 11, 11],

  [10, 10, 10, 10,
    10, 10, 10, 10,
    10, 10],

  [9, 9, 9, 9,
    9, 9, 9, 9,
    9],

  [8, 8, 8, 8,
    8, 8, 8, 8],

  [7, 7, 7, 7,
    7, 7, 7],

  [6, 6, 6, 6,
    6, 6],

  [5, 5, 5, 5,
    5],

  [4, 4, 4, 4],

  [3, 3, 3],

  [2, 2],

  [1]
];

async function delay (t, val) {
  return new Promise(function (resolve) {
    setTimeout(function () {
      resolve(val);
    }, t);
  });
} // end of delay

function isValidJSONString (str) {
  try {
    JSON.parse(str);
  } catch (e) {
    return false;
  }
  return true;
}

async function validate2fa (userId, code2fa) {
  let userObj = null;
  let result = false;

  await SQL.users.findOne({
    where: {
      u_userId: userId,
      u_isActive: true
    }
  }).then((user) => {
    userObj = user;
  }).catch((e) => {
    console.log(e);
  });

  if (userObj == null) return false;
  const type = (userObj.otp_secret === undefined) ? 'EMAIL' : 'OTP';

  if (!code2fa) return false;

  if (type === 'EMAIL') {
    if (userObj.code2fa == null) return false;

    result = (code2fa === userObj.code2fa);

    // clear code2fa, so it's not used again.
    userObj.update({
      u_code2fa: null,
      u_code2faEpoch: null
    });
  } else {
    const decryptedSecret = await aesDecrypt(userObj.otp_secret);

    result = await lib.otp.authenticator.check(code2fa, decryptedSecret);

    if (result.err) {
      console.log(result.err);
      return false;
    }
  }

  return result;
} // end of validate2fa

async function aesEncrypt (plainText, initVector = null, mode = 'CBC') {
  // Bits used will depend on key length
  // Key should be 32 bytes so that 256bit encryption is used

  // Extract key from config and convert into array of numbers
  const keyString = lib.CONFIG.AES.KEY;
  let key = keyString.split(', ');
  key = key.map(x => Number(x));

  // Extract default Initial Vector from config and convert into array of numbers
  if (initVector == null) {
    const initVectorString = lib.CONFIG.AES.INITIAL_VECTOR;
    initVector = initVectorString.split(', ');
    initVector = initVector.map(x => Number(x));
  }

  // The initialization vector (must be 16 bytes)
  if (initVector.length !== 16) {
    throw new Error('Initialization Vector must be 16 bytes');
  }

  let encryptedBytes = null;

  // Convert text to bytes (text must be a multiple of 16 bytes)
  const plainTextBytes = await lib.aes.utils.utf8.toBytes(plainText);

  switch (mode) {
    case 'CTR':
      const aesCtr = new lib.aes.ModeOfOperation.ctr(key, initVector);
      encryptedBytes = await aesCtr.encrypt(plainTextBytes);
      break;

    case 'CBC':

      // CBC Method Requires plaintext to be mutiple of 16 bytes
      const padding = PADDING[(plainTextBytes.byteLength % 16) || 0];
      const paddedPlainTextBytes = new Uint8Array(plainTextBytes.byteLength + padding.length);

      paddedPlainTextBytes.set(plainTextBytes);
      paddedPlainTextBytes.set(padding, plainTextBytes.byteLength);

      const aesCbc = new lib.aes.ModeOfOperation.cbc(key, initVector);
      encryptedBytes = await aesCbc.encrypt(paddedPlainTextBytes);
      break;
  }

  // Convert to hex for storage
  const encryptedHex = await lib.aes.utils.hex.fromBytes(encryptedBytes);

  return encryptedHex;
} // end of aesEncrypt

async function aesDecrypt (encryptedHex, initVector = null, mode = 'CBC') {
  // Bits used will depend on key length
  // Key should be 32 bytes so that 256bit encryption is used

  // Extract key from config and convert into array of numbers
  const keyString = lib.CONFIG.AES.KEY;
  let key = keyString.split(', ');
  key = key.map(x => Number(x));

  // Extract default Initial Vector from config and convert into array of numbers
  if (initVector == null) {
    const initVectorString = lib.CONFIG.AES.INITIAL_VECTOR;
    initVector = initVectorString.split(', ');
    initVector = initVector.map(x => Number(x));
  }

  // The initialization vector (must be 16 bytes)
  if (initVector.length !== 16) {
    throw new Error('Initialization Vector must be 16 bytes');
  }

  let decryptedBytes = null;

  // Convert hex back to bytes for decryption
  const encryptedBytes = await lib.aes.utils.hex.toBytes(encryptedHex);

  switch (mode) {
    case 'CTR':
      const aesCtr = new lib.aes.ModeOfOperation.ctr(key, initVector);
      decryptedBytes = await aesCtr.decrypt(encryptedBytes);
      break;

    case 'CBC':

      const aesCbc = new lib.aes.ModeOfOperation.cbc(key, initVector);
      decryptedBytes = await aesCbc.decrypt(encryptedBytes);

      decryptedBytes = decryptedBytes.subarray(0, decryptedBytes.byteLength - decryptedBytes[decryptedBytes.byteLength - 1]);
      break;
  }

  // Convert our bytes back into text
  const decryptedText = await lib.aes.utils.utf8.fromBytes(decryptedBytes);

  return decryptedText;
} // end of aesDecrypt

async function asyncForEach (array, callback) {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);
  }
}

function objArrSortFunc (key, order = 'asc') {
  // Sample usage
  // transactions.sort(objArrSortFunc('amount', 'desc'));

  return function innerSort (a, b) {
    if (!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
      // property doesn't exist on either object
      throw new Error('Object key does not exist');
    }

    const varA = (typeof a[key] === 'string') ? a[key].toUpperCase() : a[key];
    const varB = (typeof b[key] === 'string') ? b[key].toUpperCase() : b[key];

    let comparison = 0;

    if (varA > varB) {
      comparison = 1;
    } else if (varA < varB) {
      comparison = -1;
    }

    return (
      (order === 'desc') ? (comparison * -1) : comparison
    );
  };
}

function randomString (length) {
  var result = '';
  var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var charactersLength = characters.length;
  for (var i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }

  return result;
}

module.exports = {
  delay,
  isValidJSONString,
  validate2fa,
  aesEncrypt,
  aesDecrypt,
  asyncForEach,
  randomString
};
