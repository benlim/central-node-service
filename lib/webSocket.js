
const lib = require('./lib.js');
// const dashboard = require('./api/users.js')

class WebSocket {
  constructor () {
    this.initialized = false;
    this.socketIoServer = null;
    this.connectedClients = {};
  }

  initialize (server) {
    this.socketIoServer = server;
    this.initialized = true;

    // Setting up a socket with the namespace "connection" for new sockets
    this.socketIoServer.on('connection', async socket => {
      // Only accept authorized connections
      const jwtToken = socket.handshake.query.jwt;
      let result = null;
      try {
        result = lib.jwt.verify(jwtToken, lib.CONFIG.JWT.SECRET);
      } catch (err) {
        console.log(err);
        return;
      }

      // Find user object associated with jwt id
      const userObj = await lib.SQL.users.findOne({ where: { u_id: result.id } });

      if (this.connectedClients[userObj.u_userId]) {
        console.log('Force Disconnect Socket ID: ' + this.connectedClients[userObj.u_userId].id);
        this.connectedClients[userObj.u_userId].disconnect();
      }

      this.connectedClients[userObj.u_userId] = socket;

      console.log((new Date()) + ' Recieved a new connection from origin ' + socket.handshake.headers.origin + '.');
      console.log('User ID: ' + userObj.u_userId + ' || ' + 'Socket ID: ' + socket.id);

      this.initialEmit(userObj.u_userId);

      // //Here we listen on a new namespace called "incoming data"
      // socket.on("incoming data", (data)=>{
      //     //Here we broadcast it out to all other sockets EXCLUDING the socket which sent us the data
      //     // socket.broadcast.emit("outgoing data", {num: data});
      // });

      // A special namespace "disconnect" for when a client disconnects
      socket.on('disconnect', data => {
        console.log('Client Disconnected. Socket ID: ' + socket.id);
        delete this.connectedClients[userObj.u_userId];
      });
    });
  }

  isClientConnected (userId) {
    return (this.connectedClients[userId] !== undefined);
  }

  emitData (params) {
    if (params.userId === undefined) {
      throw new Error('Must provide User ID');
    }

    if (this.connectedClients[params.userId] !== undefined) {
      this.connectedClients[params.userId].emit('fromApi', params);
    }
  }

  async initialEmit () {

    // let emitJson = {
    //     userId: params.userId,
    //     data: {
    //         orderCount: {
    //             total: data.orderCount,
    //             confirmed: data.confirmCount,
    //             pending: data.pendingCount
    //         }
    //     }
    // }

    // this.emitData(emitJson);
  }
}

module.exports = WebSocket;
