const lib = require('./lib.js');
const SQL = lib.SQL;

// Telegram-specific requirements
const {
  TelegramClient
} = require('messaging-api-telegram');
const client = TelegramClient.connect(lib.CONFIG.TELEGRAM.KEY);
const autoSend = lib.CONFIG.TELEGRAM.STATUS;

async function sendGrpMessage (message, chatId = lib.CONFIG.TELEGRAM.GROUP) {
  if (autoSend) {
    client.sendMessage(chatId, message, {
      disable_web_page_preview: true,
      disable_notification: true,
      parse_mode: 'markdown'
    }).then((response) => {

    }).catch((e) => {
      console.log('-E- ' + e);
      return {
        error: e
      };
    }); // end of catch
  }
  return true;
}

async function listen () {
  var offset = null; // declare
  while (true) {
    await SQL.telegram_bots.findOne({
      where: {
        update_id: {
          [lib.Op.ne]: null
        }
      }
    }).then(async id => {
      offset = Number(id.update_id) + 1;
      await client.getUpdates({
        offset: offset,
        limit: 100
      }).then(async (response) => {
        if (response.length < 1) {
          return Promise.resolve;
        }
        await response.forEach(async updates => {
          if (updates.message === undefined) {
            return;
          }
          if ((updates.message.chat.id === lib.CONFIG.TELEGRAM.GROUP) && (updates.message.text !== undefined)) {
            if (updates.message.text === '/stats') {
              // await groupReportStats()
            }

            if (updates.message.text === '/balance') {
              // await groupReportBal()
            }

            if (updates.message.text === '/deficit') {
              // await groupReportDeficit()
            }
          }
        });
        await id.update({
          update_id: response[response.length - 1].update_id
        });
      }).catch((e) => {
        console.log('-E- ' + e);
      }); // end of catch
    }); // end of client.getUpdates
  }
} // end of listen

// Listens and responds to command in group.
// listen()

module.exports = {
  sendGrpMessage: async function (message, chatId = lib.CONFIG.TELEGRAM.GROUP) {
    return await sendGrpMessage(message, chatId);
  }
};
