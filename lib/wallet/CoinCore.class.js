const lib = require('../lib.js');
const utility = require('../utility.js');
const Bitcore = require('bitcoin-core');
const BLOCK_INFO = 'https://blockchain.info';
const bitcoin = require('bitcoinjs-lib');
const net = bitcoin.networks.bitcoin;
// const SQL = lib.SQL;

/*
* Parent class for all OMNI Token Coin Cores
* Abstract Class to enforce certain functions to be implemented in children
* Contains general functions that can be used by all child classes
*/

class CoinCore {
  constructor () {
    this.auto_send = false; // Switch to false to prevent send transaction

    this.coreType = 'BTC';

    this.client = new Bitcore({
      network: lib.CONFIG.BTC_NODE.NETWORK,
      port: lib.CONFIG.BTC_NODE.PORT,
      host: lib.CONFIG.BTC_NODE.HOST,
      username: lib.CONFIG.BTC_NODE.USER,
      password: lib.CONFIG.BTC_NODE.PASSWORD
    });

    if (this.constructor === CoinCore) {
      throw new TypeError('Abstract class "CoinCore" cannot be instantiated directly.');
    }

    if (this.estimateFee === undefined) {
      throw new TypeError('Classes extending the CoinCore Abstract Class must implement the method estimateFee');
    }

    if (this.getPrivateKey === undefined) {
      throw new TypeError('Classes extending the CoinCore Abstract Class must implement the method getPrivateKey');
    }

    if (this.importPrivateKey === undefined) {
      throw new TypeError('Classes extending the CoinCore Abstract Class must implement the method importPrivateKey');
    }

    if (this.createAcc === undefined) {
      throw new TypeError('Classes extending the CoinCore Abstract Class must implement the method createAcc');
    }

    if (this.getTransaction === undefined) {
      throw new TypeError('Classes extending the CoinCore Abstract Class must implement the method getTransaction');
    }

    if (this.getPendingTx === undefined) {
      throw new TypeError('Classes extending the CoinCore Abstract Class must implement the method getPendingTx');
    }

    if (this.listTransactions === undefined) {
      throw new TypeError('Classes extending the CoinCore Abstract Class must implement the method listTransactions');
    }

    if (this.listAccounts === undefined) {
      throw new TypeError('Classes extending the CoinCore Abstract Class must implement the method listAccounts');
    }

    if (this.getBalance === undefined) {
      throw new TypeError('Classes extending the CoinCore Abstract Class must implement the method getBalance');
    }

    if (this.getWalletBalances === undefined) {
      throw new TypeError('Classes extending the CoinCore Abstract Class must implement the method getWalletBalances');
    }

    if (this.getWalletTotal === undefined) {
      throw new TypeError('Classes extending the CoinCore Abstract Class must implement the method getWalletTotal');
    }

    if (this.sendTransaction === undefined) {
      throw new TypeError('Classes extending the CoinCore Abstract Class must implement the method sendTransaction');
    }

    if (this.computeAddressReceivedSQL === undefined) {
      throw new TypeError('Classes extending the CoinCore Abstract Class must implement the method computeAddressReceivedSQL');
    }
  }

  delay (t, val) {
    return new Promise(function (resolve) {
      setTimeout(function () {
        resolve(val);
      }, t);
    });
  } // end of delay

  validate (result) {
    if (result === undefined) {
      return {
        error: 'Please specify rpc result in argument.'
      };
    }
    if (JSON.stringify(result).toLowerCase().includes('rpcerror')) {
      return {
        error: '-E- ' + result
      };
    } else {
      return result;
    }
  }

  async getBtcPrice (convert = 'USD') {
    if (typeof convert !== 'string') {
      return {
        error: 'Invalid input conversion value.'
      };
    }

    var priceObj = await lib.axios.get(`${BLOCK_INFO}/ticker`);

    try {
      const data = priceObj.data;
      if (data[convert].last === undefined) {
        return {
          error: 'Conversion pair is not recognised, please try again later.'
        };
      } else {
        return data[convert].last;
      }
    } catch (err) {
      return {
        error: err
      };
    }
  } // end of getBtcPrice

  convertSatoshiToBtc (satoshi) {
    if (satoshi === 0) {
      return satoshi;
    } else {
      return (satoshi / 100000000);
    }
  }

  async getPrivateKey (params) {
    console.log('getPrivateKey_' + this.coreType);

    const address = params.address;

    // prep rpc command
    var batch = [{
      method: 'dumpprivkey',
      parameters: [address]
    }];

    var res = await this.client.command(batch);
    if (this.validate(res).error) {
      return this.validate(res);
    }

    // console.log('dump', res)
    return res[0];
  }

  async importPrivateKey (params) {
    console.log('importPrivateKey_' + this.coreType);

    const privateKey = params.privateKey;
    const address = params.address;
    const rescan = params.rescan || false;

    // prep rpc command
    var batch = [{
      method: 'importprivkey',
      parameters: [privateKey, address, rescan]
    }];

    var res = await this.client.command(batch);
    if (this.validate(res).error) {
      return this.validate(res);
    }
    // console.log(res)
    return res;
  }

  async createAcc (params) {
    console.log('createAcc_' + this.coreType);

    // generate address details
    var walletData = {}; // declare
    var netObj = {
      network: net
    };

    var keyPair = bitcoin.ECPair.makeRandom(netObj);

    const { address } = bitcoin.payments.p2pkh({
      pubkey: keyPair.publicKey
    });

    var pk = keyPair.toWIF();

    walletData.address = address;
    walletData.privateKey = pk; // get Wallet input format

    const importParams = {
      privateKey: walletData.privateKey,
      address: walletData.address
    };
    var res = await this.importPrivateKey(importParams);
    if (res.error) {
      return res;
    }
    // console.log(res)
    return {
      address: walletData.address,
      privateKey: walletData.privateKey
    };
  }

  async validateAddr (params) {
    console.log('validateAddr_' + this.coreType);

    const address = params.address;

    const valid = await lib.walletValid.validate(address, 'BTC');

    // console.log(res[0])
    return valid;
  }

  // Check if address exists in wallet.dat
  async isAddressInWallet (params) {
    const address = params.address;

    // prep rpc command
    var batch = [{
      method: 'getaccount',
      parameters: [address]
    }];

    var result = await this.client.command(batch);
    this.validate(result);

    if (result[0].length > 0) {
      return true;
    } else {
      return false;
    }
  }

  // List all accounts in wallet.dat
  async listAccounts (params) {
    console.log('listAccounts' + this.coreType);

    // prep rpc command
    var batch = [{
      method: 'listlabels',
      parameters: []
    }];

    var res = await this.client.command(batch);
    if (this.validate(res).error) {
      return this.validate(res);
    }

    // console.log(res[0])
    return res[0];
  }

  // Determine token/coin type of transaction
  async getTokenType (params) {
    // console.log('getTokenType' + '_' + this.coreType + '_' + this.tokenType);

    const txid = params.txid;

    // prep rpc command
    var batch = [{
      method: 'omni_gettransaction',
      parameters: [txid]
    }];

    var res = await this.client.command(batch);
    if (this.validate(res).error) {
      // If rpc error returns the following message then return token type as BTC instead of error
      if (this.validate(res).error.toLowerCase().includes('no omni layer protocol transaction')) {
        return 'BTC';
      }

      return this.validate(res);
    }

    if (res[0].propertyid === this.coinPropertyId) {
      return this.tokenType;
    }

    // If property id does not match current core property id then return false
    return false;
  }

  // Get block info of transaction
  async getBlockInfo (params) {
    // console.log('getBlockInfo' + '_' + this.coreType + '_' + this.tokenType);

    const txid = params.txid || null;
    const blockNum = params.blockNum || null;
    let blockHash = params.blockHash || null;

    if (blockHash == null) {
      if (txid) {
        const batch = [{
          method: 'gettransaction',
          parameters: [txid]
        }];

        const res = await this.client.command(batch);
        if (this.validate(res).error) {
          return this.validate(res);
        }

        blockHash = res[0].blockhash;
      }

      if (blockNum) {
        const batch = [{
          method: 'getblockhash',
          parameters: [blockNum]
        }];

        const res = await this.client.command(batch);

        if (this.validate(res).error) {
          return this.validate(res);
        }

        blockHash = res[0];
      }
    }

    // prep rpc command
    var batch = [{
      method: 'getblock',
      parameters: [blockHash]
    }];

    var res = await this.client.command(batch);
    if (this.validate(res).error) {
      return this.validate(res);
    }

    return res[0];
  }
}

module.exports = {
  CoinCore,
  lib,
  utility
};
