const lib = require('../lib.js');
const Web3 = require('web3');
const SQL = lib.SQL;

/*
* Parent class for Ethereum based transactions
* Main class containing functions for manipulating and getting information on ETH in the Ethereum Blockchain
* Contains general functions that can be used by all child classes
*/

class EthereumCore {
  constructor () {
    this.auto_send = true; // Switch to false to prevent send transaction

    this.coreType = 'ETH';
    this.tokenType = 'ETH';

    this.rpcURL = lib.CONFIG.ETH_NODE.NETWORK;
    this.rpcWebSocketUrl = lib.CONFIG.ETH_NODE.WEB_SOCKET;

    const OPTIONS = {
      defaultBlock: 'latest',
      transactionConfirmationBlocks: 1,
      transactionBlockTimeout: 5
    };

    this.client = new Web3(this.rpcURL, null, OPTIONS);

    this.walletPassword = lib.CONFIG.ETH_WALLET_PASSWORD;

    this.unitMapping = {
      0: 'noether',
      1: 'wei',
      3: 'kwei',
      6: 'mwei',
      9: 'gwei',
      12: 'microether',
      15: 'milliether',
      18: 'ether',
      21: 'kether',
      24: 'mether',
      27: 'tether'
    };
  }

  delay (t, val) {
    return new Promise(function (resolve) {
      setTimeout(function () {
        resolve(val);
      }, t);
    });
  }

  async getTokenType (params) {
    // console.log('getTokenType' + this.coreType + '_' + this.tokenType);

    // const node = params.node;
    const txid = params.txid;

    const tx = await this.client.eth.getTransaction(txid);

    const toAddress = tx.to;

    const code = await this.client.eth.getCode(toAddress);

    if (code === '0x') {
      return 'ETH';
    }

    // If not ETH transaction then return false
    if (this.tokenAddress === undefined) {
      return false;
    }

    if (toAddress.toLowerCase() === this.tokenAddress.toLowerCase()) {
      return this.tokenType;
    }

    return false;
  }

  async getEthPrice (convert = 'USD') {
    const apiKey = lib.CONFIG.ETHERSCAN.APIKEY;
    const apiUrl = 'https://api.etherscan.io/api?module=stats&action=ethprice&apikey=' + apiKey;

    let result = await lib.axios.get(apiUrl);
    result = result.data;

    let response = {
      USD: result.result.ethusd
    };

    response = response[convert];

    return response;
  }

  async getMedianGasPrice (params) {
    // The gas price is determined by the last few blocks median gas price. GasPrice is the wei per unit of gas
    const medianGasPrice = await this.client.eth.getGasPrice();

    return medianGasPrice;
  }

  async getConfirmations (params) {
    const txid = params.txid || null;
    let blockNumber = params.blockNumber || null;

    if (blockNumber == null) {
      // Get transaction details
      const tx = await this.client.eth.getTransaction(txid);
      blockNumber = (tx.blockNumber === undefined) ? null : tx.blockNumber;
    }

    // Get current block number
    const currentBlock = await this.client.eth.getBlockNumber();

    // When transaction is unconfirmed, its block number is null.
    // In this case we return 0 as number of confirmations
    return blockNumber === null ? 0 : currentBlock - blockNumber;
  }

  async getBlockInfo (params) {
    // console.log('getBlockInfo' + this.coreType + '_' + this.tokenType);

    const txid = params.txid || null;
    let blockNum = params.blockNum || null;
    const blockHash = params.blockHash || null;

    if (blockNum == null && blockHash == null) {
      const result = await this.client.eth.getTransaction(txid);

      blockNum = result.blockNumber;
    }

    const blockInfo = await this.client.eth.getBlock(blockNum || blockHash);

    return blockInfo;
  }

  // Get all addresses in wallet
  async getWalletAddresses () {
    const result = await SQL.ethereum_keys.findAll({ attributes: [['ek_address', 'paymentAddr']], raw: true });

    return result;
  }

  // Validate address if is a valid Ethereum address
  async validateAddr (params) {
    const address = params.address;

    const valid = this.client.utils.isAddress(address);

    return valid;
  }

  // Check if address exists in wallet
  async isAddressInWallet (params) {
    const address = params.address;
    const transaction = params.transaction || null;

    let result = null;

    if (transaction) {
      result = await SQL.ethereum_keys.findOne({ where: { ek_address: address }, transaction: transaction });
    } else {
      result = await SQL.ethereum_keys.findOne({ where: { ek_address: address } });
    }

    if (result != null) {
      return true;
    } else {
      return false;
    }
  }

  async estimateEtherCost (params) {
    // console.log('estimateEtherCost_' + this.coreType + '_' + this.tokenType);

    const sender = params.sender;
    const recipient = params.recipient;
    let amount = params.amount;

    // If amount is number convert to string
    if (!isNaN(amount)) {
      amount = amount.toString();
    }

    // Convert amount to Wei
    const weiAmount = this.client.utils.toWei(amount, 'ether');

    // The gas price is determined by the last few blocks median gas price. GasPrice is the wei per unit of gas
    const medianGasPrice = await this.client.eth.getGasPrice();

    // Determine the nonce
    const count = await this.client.eth.getTransactionCount(sender);

    const txObj = {
      nonce: this.client.utils.toHex(count),
      from: sender,
      to: recipient,
      value: this.client.utils.toHex(weiAmount),
      gasPrice: this.client.utils.toHex(medianGasPrice)
    };

    let gasEstimate = await this.client.eth.estimateGas(txObj) * 1.1;
    gasEstimate = Math.ceil(gasEstimate);

    const totalWeiCost = this.client.utils.toBN(gasEstimate * medianGasPrice);

    const etherCost = this.client.utils.fromWei(totalWeiCost, 'ether');

    return etherCost;
  }

  async estimateFee (params) {
    // console.log('estimateFee_' + this.coreType + '_' + this.tokenType);

    const etherCost = await this.estimateEtherCost(params);

    const ethFee = etherCost * (await this.getEthPrice('USD'));

    return ethFee;
  }

  async getPrivateKey (params) {
    console.log('getPrivateKey_' + this.coreType + '_' + this.tokenType);

    const address = params.address;

    const result = await SQL.ethereum_keys.findOne({
      where: {
        ek_address: address
      }
    });

    if (result == null) {
      throw new Error('Private Key does not exist');
    }

    const password = this.walletPassword;

    const account = this.client.eth.accounts.decrypt(result.ek_keyJson, password);

    return account;
  }

  async importPrivateKey (params) {
    console.log('importPrivateKey_' + this.coreType + '_' + this.tokenType);

    const privateKey = params.privateKey;
    // const address = params.address;

    const password = this.walletPassword;

    const keyJson = this.client.eth.accounts.encrypt(privateKey, password);

    // const currEpoch = Math.floor(new Date() / 1000);

    const _this = this;

    // Do not commit to DB if error occurs within transaction
    await SQL.db.transaction(async function (t) {
      const checksumAddress = _this.client.utils.toChecksumAddress(keyJson.address);

      await SQL.ethereum_keys.create({
        ek_version: keyJson.version,
        ek_id: keyJson.id,
        ek_address: checksumAddress,
        ek_keyJson: JSON.stringify(keyJson)
      }, { transaction: t });

      const params = {
        address: checksumAddress,
        transaction: t
      };

      if (!await _this.isAddressInWallet(params)) {
        throw new Error('Address not found in wallet!');
      }

      return Promise.resolve();
    });
  }

  // faster account generation
  async createAcc (params) {
    console.log('createAcc_' + this.coreType + '_' + this.tokenType);

    // generate address details
    const account = await this.client.eth.accounts.create();

    params = {
      address: account.address,
      privateKey: account.privateKey
    };

    // await this.importPrivateKey(params);

    return {
      address: account.address,
      privateKey: account.privateKey
    };
  }

  async getTransaction (params) {
    // console.log('getTransaction_' + this.coreType + '_' + this.tokenType);

    const txid = params.txid;

    let result = null;

    // Transaction will not appear immediately. Try for certain amount of times until info is available.
    for (let i = 1; i <= 20; i++) {
      await this.delay(5000);
      result = await this.client.eth.getTransaction(txid);
      if (result != null) {
        // console.log("Number of tries:" + i);
        break;
      }
    }

    if (result == null) {
      throw new Error('Unable to get transaction info');
    }
    // console.log(result);
    result.receiveAddress = result.to;
    result.amount = this.client.utils.fromWei(result.value, 'ether');

    const confParams = {
      txid: txid,
      blockNumber: result.blockNumber
    };
    result.confirmations = await this.getConfirmations(confParams);

    let status = false;
    let gasUsed = null;

    if (result.confirmations > 0) {
      const txReceipt = await this.client.eth.getTransactionReceipt(txid);
      status = txReceipt.status;
      gasUsed = txReceipt.gasUsed;
    }

    result.gasUsed = gasUsed;

    result.valid = status;

    result.fee = null;

    if (gasUsed != null) {
      const gasPriceGwei = this.client.utils.fromWei(result.gasPrice, 'gwei');

      const feeWei = this.client.utils.fromWei((parseFloat(gasUsed) * parseFloat(gasPriceGwei)).toFixed(0), 'gwei');
      // let feeWei = this.client.utils.fromWei(this.client.utils.toBN(gasUsed).mul(this.client.utils.toBN(gasPriceGwei)), 'gwei');

      result.fee = feeWei;
    }

    return result;
  }

  // address needs to be from wallet.
  // Returns complete transactions for wallets that are generated and imported to wallet.dat since usage.
  async listTransactions (params) {
    console.log('listTransactions_' + this.coreType + '_' + this.tokenType);

    const address = params.address;

    // prep rpc command
    var batch = [{
      method: 'omni_listtransactions',
      parameters: [address, 99999999, 0, 556128]
    }];

    var res = await this.client.command(batch);
    this.validate(res);

    // console.log(res)
    return res[0];
  }

  // List all accounts in wallet.dat
  async listAccounts (params) {
    // console.log('listAccounts_' + this.coreType + '_' + this.tokenType);

    const walletAddresses = await this.getWalletAddresses();

    const returnObj = [];

    for (const address of walletAddresses) {
      returnObj.push(address.paymentAddr);
    }

    return returnObj;
  }

  async getBalance (params) {
    // console.log('getBalance_' + this.coreType + '_' + this.tokenType);

    const address = params.address || null;

    if (address == null) {
      throw new Error('No address provided');
    }

    let result = null;

    if (Array.isArray(params.address)) {
      result = [];

      const batch = new this.client.BatchRequest();

      for (const address of params.address) {
        batch.add(this.client.eth.getBalance.request(address, (err, res) => { if (err) throw err; }));
      }

      const balances = await batch.execute();

      let cnt = 0;

      for (const address of params.address) {
        const balance = this.client.utils.fromWei(balances.response[cnt], 'ether');

        result[address] = balance;

        cnt++;
      }
    } else {
      const weiBalance = await this.client.eth.getBalance(address);
      const balance = this.client.utils.fromWei(weiBalance, 'ether');

      result = balance;
    }

    // console.log(res[0])
    return result;
  }

  async getWalletBalances (params) {
    // console.log('getWalletBalances_' + this.coreType + '_' + this.tokenType);

    const walletAddresses = await this.getWalletAddresses();

    const batch = new this.client.BatchRequest();

    for (const addrData of walletAddresses) {
      batch.add(this.client.eth.getBalance.request(addrData.paymentAddr, (err, res) => { if (err) throw err; }));
    }

    const result = await batch.execute();

    const formattedBalances = [];

    for (const key in walletAddresses) {
      const resultBal = result.response[key] || 0;

      const checkSumAddress = this.client.utils.toChecksumAddress(walletAddresses[key].paymentAddr);

      formattedBalances[checkSumAddress] = this.client.utils.fromWei(resultBal, 'ether');
    }

    return formattedBalances;
  }

  // Sends push tx with funder for btc fee if funder is specified.
  // funder, recipient in run.config
  async sendTransaction (params) {
    console.log('sendTransaction_' + this.coreType + '_' + this.tokenType);

    // declare
    const sender = params.sender;
    const recipient = params.recipient;
    let amount = params.amount;
    let gasLimit = params.gasLimit || null;

    if (!this.client.utils.isAddress(sender) || !this.client.utils.isAddress(recipient)) {
      throw new Error('Invalid address as sender or recipient');
    }

    // If amount is number convert to string
    if (!isNaN(amount)) {
      amount = amount.toString();
    }

    const senderWeiBalance = await this.client.eth.getBalance(sender);
    const senderBalance = this.client.utils.fromWei(senderWeiBalance, 'ether');

    if (Number(senderBalance === 0)) {
      throw new Error('Sender balance is 0');
    }

    if (Number(senderBalance) < Number(amount)) {
      throw new Error('Not enough funds in sender address');
    }

    // Convert amount to Wei
    const weiAmount = this.client.utils.toWei(amount, 'ether');

    // The gas price is determined by the last few blocks median gas price. GasPrice is the wei per unit of gas
    const medianGasPrice = await this.client.eth.getGasPrice();

    let hash = null;

    await SQL.db.transaction(async t1 => {
      let nodeNonce = null;
      let currNonce = null;
      let nonceRecord = null;

      // Determine the nonce
      if (sender === lib.CONFIG.ETH_WALLET_ADDR.WITHDRAW) {
        nonceRecord = await SQL.configs.findOne({
          where: { conf_code: 'ETH_HOT_WALLET_NONCE' },
          transaction: t1,
          lock: t1.LOCK.UPDATE
        });

        currNonce = nonceRecord.conf_value;

        nodeNonce = await this.client.eth.getTransactionCount(sender);

        if (nodeNonce > currNonce) {
          currNonce = nodeNonce;
        }
      } else {
        currNonce = await this.client.eth.getTransactionCount(sender);
      }

      const txObj = {
        nonce: this.client.utils.toHex(currNonce),
        from: sender,
        to: recipient,
        value: this.client.utils.toHex(weiAmount),
        gasPrice: this.client.utils.toHex(medianGasPrice)
      };

      if (gasLimit == null) {
        gasLimit = Math.ceil(await this.client.eth.estimateGas(txObj) * 1.1);
      }

      txObj.gasLimit = gasLimit;

      const balParams = {
        address: sender
      };

      const balance = await this.getBalance(balParams);

      // If want to send ALL remaining funds from address
      // Need to reduce amount sent to allow for gas cost
      if (amount === balance) {
        const estimatedWeiCost = gasLimit * medianGasPrice;

        const newValue = weiAmount - estimatedWeiCost;
        txObj.value = this.client.utils.toHex(newValue);
      }

      // console.log(txObj);

      const result = await SQL.ethereum_keys.findOne({
        where: {
          ek_address: sender
        }
      });

      const password = this.walletPassword;

      const account = this.client.eth.accounts.decrypt(result.ek_keyJson, password);
      // console.log(txObj);
      const signedTx = await account.signTransaction(txObj);
      // console.log(signedTx)

      await result.update({ ek_lastSigned: new Date() });

      if (!this.auto_send) {
        return {
          skip: 'Send transaction is switched off by auto_send:false'
        };
      }

      this.client.eth.sendSignedTransaction(signedTx.rawTransaction, async (error, result) => {
        if (error) {
          console.log(error);

          const watchedAddr = await SQL.watch_addresses.findOne({
            where: {
              wa_address: sender
            }
          });

          const user = watchedAddr.wa_userId;

          const userObj = await SQL.users.findOne({
            where: {
              u_userId: user
            }
          });

          const callbackUrl = userObj.u_callbackUrl;

          if (callbackUrl == null) {
            return;
          }

          const options = {
            method: 'POST',
            uri: callbackUrl,
            headers: {
              'Content-type': 'application/json',
              Accept: 'application/json',
              'Accept-Charset': 'utf-8'
            },
            body: {
              txId: signedTx.transactionHash,
              success: false,
              error: error
            },
            json: true // Automatically parses the JSON string in the response
          };

          lib.requestpn(options).then(result => {
            return result;
          }).catch(err => {
            throw err;
          });
        } else {
          console.log(result);
        }
      });

      hash = signedTx.transactionHash;

      // //call fetch transaction details to confirm it is recorded.
      // let txParam = {
      //     txid: hash
      // }

      // var resHash = await this.getTransaction(txParam);

      // if(!resHash){
      //     throw new Error("Transaction was not recorded on blockchain");
      // }

      if (sender === lib.CONFIG.ETH_WALLET_ADDR.WITHDRAW) {
        await nonceRecord.update({
          conf_value: parseFloat(currNonce) + 1
        }, { transaction: t1 });
      }

      return Promise.resolve();
    }).then(result => {

    }).catch(err => {
      throw err;
    });

    return hash;
  }

  async computeAddressReceivedSQL (params) {
    // console.log('computeAddressReceivedSQL_' + this.coreType + '_' + this.tokenType);

    const address = params.address;

    if (!address || typeof address !== 'string') {
      // console.log("-E- txid param not valid.")
      return {
        error: 'Please make sure that argument specified is a string.'
      };
    }

    // fetch all scheduled forward tx that are not equals to INVALID
    var totalAmt = 0;
    totalAmt = await SQL.usdt_clear_batches.sum('amount', {
      where: {
        node: this.coreType,
        token: this.tokenType,
        paymentAddr: address,
        status: {
          [lib.Op.notIn]: ['INVALID', 'OPEN']
        }
      }
    });

    if (!totalAmt) {
      totalAmt = 0;
    }

    return totalAmt;
  }

  async confirmTransaction (txObj, confirmations = 1) {
    // Wait 30 seconds before running code
    setTimeout(async () => {
      const txid = txObj.tx_transactionId;

      const params = {
        txid: txid
      };

      // Get current number of confirmations and compare it with sought-for value
      const trxConfirmations = await this.getConfirmations(params);

      if (trxConfirmations >= confirmations) {
        const txReceipt = await this.client.eth.getTransactionReceipt(txid);

        const gasPriceGwei = this.client.utils.fromWei(Number(txObj.tx_ethGasPrice).toFixed(0), 'gwei');
        const feeWei = this.client.utils.fromWei((txReceipt.gasUsed * gasPriceGwei).toFixed(0), 'gwei');

        let ethPrice = await lib.SQL.configs.findOne({ where: { conf_cat1: 'PRICE', conf_cat2: this.coreType, conf_cat3: 'USD' } });
        ethPrice = ethPrice.conf_value;
        const networkFeeUsd = ethPrice * feeWei;

        await lib.SQL.transactions.update({
          tx_status: 'SUCCESS',
          tx_networkFee: feeWei,
          tx_networkFeeUsd: networkFeeUsd,
          tx_ethGasUsed: txReceipt.gasUsed,
          tx_confirmedAt: new Date()
        }, {
          where: {
            tx_transactionId: txid
          }
        });

        // Call user callback url
        const wallet = require('../api/wallet.js');
        wallet.userCallbackUrl(txid);

        return;
      }

      // Recursive call
      return this.confirmTransaction(txObj, confirmations);
    }, 30 * 1000);
  }

  async watchPendingTx (params) {
    const method = params.method || 'block';

    if (method === 'block') {
      this.watchBlocks();
    } else {
      // Instantiate web3 with WebSocket provider
      const subWeb3 = new Web3(new Web3.providers.WebsocketProvider(this.rpcWebSocketUrl));

      // Instantiate subscription object
      subWeb3.eth.subscribe('pendingTransactions', function (error, result) {
        if (error) throw error;
      })
        .on('data', async (txid) => {
          // Get transaction details
          const trx = await this.client.eth.getTransaction(txid);

          if (trx === null || trx.to === null) {
            return;
          };

          const watchedAddress = await lib.SQL.watch_addresses.findOne({
            where: {
              [lib.Op.or]: [{ wa_address: trx.to }, { wa_address: trx.from }]
            }
          });

          // Do nothing if transaction not linked to watched address
          if (watchedAddress === null) {
            return;
          }

          // Perform actions for pending transaction
          let existingTx = await lib.SQL.transactions.findOne({
            where: {
              tx_transactionId: txid
            }
          });

          if (existingTx === null) {
            existingTx = await lib.SQL.transactions.create({
              tx_node: this.coreType,
              tx_token: this.tokenType,
              tx_transactionId: txid,
              tx_status: 'PENDING',
              tx_amount: this.client.utils.fromWei(trx.value, 'ether'),
              tx_senderAddr: trx.from,
              tx_recipientAddr: trx.to,
              tx_ethGasPrice: trx.gasPrice,
              tx_ethNonce: trx.nonce
            });
          }

          // Wait for transaction confirmation then perform actions for confirmed transaction
          this.confirmTransaction(existingTx);

          // Unsubscribe from pending transactions.
          // subscription.unsubscribe();
        });
    }
  }

  async watchBlocks () {
    // Instantiate web3 with WebSocket provider
    const subWeb3 = new Web3(new Web3.providers.WebsocketProvider(this.rpcWebSocketUrl));

    // Instantiate subscription object
    subWeb3.eth.subscribe('newBlockHeaders', function (error, result) {
      if (error) throw error;
    })
      .on('data', async (block) => {
        const blockNumber = block.number - 1;

        let blockInfo = null;

        // Sometimes getBlock will return null. Try until blockInfo not null
        while (blockInfo == null) {
          // 2nd Parameter set to true to return transaction objects
          blockInfo = await this.client.eth.getBlock(blockNumber, true);

          if (blockInfo == null) await this.delay(1000);
        }

        for (const txObj of blockInfo.transactions) {
          const watchedAddress = await lib.SQL.watch_addresses.findOne({
            where: {
              [lib.Op.or]: [{ wa_address: txObj.to }, { wa_address: txObj.from }]
            }
          });

          // Do nothing if transaction not linked to watched address
          if (watchedAddress === null) {
            continue;
          }

          const nonEthTx = await lib.SQL.transactions.findOne({
            where: {
              tx_transactionId: txObj.hash,
              tx_token: {
                [lib.Op.ne]: 'ETH'
              }
            }
          });

          // If transaction Id already exists and it is non ETH then skip
          if (nonEthTx != null) {
            continue;
          }

          // Will use Sequelize defined unique key to search if row exists. (In this case tx_transactionId)
          await lib.SQL.transactions.upsert({
            tx_node: this.coreType,
            tx_token: this.tokenType,
            tx_transactionId: txObj.hash,
            tx_status: 'PENDING',
            tx_amount: this.client.utils.fromWei(txObj.value, 'ether'),
            tx_senderAddr: txObj.from,
            tx_recipientAddr: txObj.to,
            tx_ethGasPrice: txObj.gasPrice,
            tx_ethNonce: txObj.nonce,
            tx_confirmedAt: new Date(Number(blockInfo.timestamp) * 1000)
          });

          await lib.SQL.configs.update(
            {
              conf_val: blockNumber,
              conf_updatedAt: new Date()
            },
            {
              where: {
                conf_cat1: 'LAST_BLOCK',
                conf_cat2: this.coreType
              }
            }
          );

          const existingTx = await lib.SQL.transactions.findOne({
            where: {
              tx_transactionId: txObj.hash
            }
          });

          // Wait for transaction confirmation then perform actions for confirmed transaction
          this.confirmTransaction(existingTx);
        }
      });
  }
}

module.exports = {
  EthereumCore,
  lib
};
