const CoinCore = require('./CoinCore.class.js').CoinCore;
const lib = require('./CoinCore.class.js').lib;
// const bitcore = require('bitcoin-core');
// const bitcoin = require('bitcoinjs-lib');
// const net = bitcoin.networks.bitcoin;
const SQL = lib.SQL;

/*
* Coin Core for OMNI USDT
* Contains all functions related to manipulating and getting information on OMNI USDT in the Bitcoin blockchain
*/

// eslint-disable-next-line camelcase
class CoinCore_BTC_USDT extends CoinCore {
  constructor () {
    super();

    this.tokenType = 'USDT';
    this.coinPropertyId = 31; // USDT Property ID
  }

  validate (result) {
    if (result === undefined) {
      throw new Error('Please specify rpc result in argument.');
    }
    if (JSON.stringify(result).toLowerCase().includes('rpcerror')) {
      throw new Error('-E- ' + result);
    } else {
      return result;
    }
  }

  // estimate fee bitcoin fees in bytes
  // default 405 bytes per transaction for (funding type)
  // default estimate fee for transaction to be mined within 6 blocks (10mins per block)
  async estimateFee (params) {
    // console.log('estimateFee_' + this.coreType + '_' + this.tokenType);

    const size = params.size || 405;
    const blocks = params.blocks || 6;

    // prep rpc command
    var batch = [{
      method: 'estimatesmartfee',
      parameters: [blocks]
    }];

    var res = await this.client.command(batch);
    this.validate(res);

    var feeBtc = res[0] * (size / 1000);
    var btcPrice = await this.getBtcPrice();
    var feeUsd = parseFloat(feeBtc * btcPrice).toFixed(2);

    return feeUsd;
  }

  // Returns error if txid is not omni type
  async getTransaction (params) {
    // console.log('getTransaction_' + this.coreType + '_' + this.tokenType);

    const txid = params.txid;

    // prep rpc command
    var batch = [{
      method: 'omni_gettransaction',
      parameters: [txid]
    }];

    var res = await this.client.command(batch);
    this.validate(res);

    // console.log(res[0])
    return res[0];
  }

  // Returns error if txid is not omni type
  async getPendingTx (params) {
    // console.log('getPendingTx_' + this.coreType + '_' + this.tokenType);

    const address = params.address;

    // prep rpc command
    var batch = [{
      method: 'omni_listpendingtransactions',
      parameters: [address]
    }];

    var res = await this.client.command(batch);
    this.validate(res);

    // console.log(res[0])
    return res[0];
  }

  // address needs to be from wallet.
  // Returns complete transactions for wallets that are generated and imported to wallet.dat since usage.
  async listTransactions (params) {
    console.log('listTransactions_' + this.coreType + '_' + this.tokenType);

    const address = params.address;

    // prep rpc command
    var batch = [{
      method: 'omni_listtransactions',
      parameters: [address, 99999999, 0, 556128]
    }];

    var res = await this.client.command(batch);
    this.validate(res);

    // console.log(res)
    return res[0];
  }

  async getBalance (params) {
    // console.log('getBalance_' + this.coreType + '_' + this.tokenType);

    const address = params.address || null;

    if (address == null) {
      throw new Error('No address provided');
    }

    let result = null;
    let batch, res;

    if (Array.isArray(params.address)) {
      result = [];

      for (const address of params.address) {
        batch = [{
          method: 'omni_getbalance',
          parameters: [address, this.coinPropertyId]
        }];

        res = await this.client.command(batch);
        this.validate(res);

        result[address] = res[0].balance;

        await this.delay(2);
      }
    } else {
      batch = [{
        method: 'omni_getbalance',
        parameters: [address, this.coinPropertyId]
      }];

      res = await this.client.command(batch);
      this.validate(res);

      result = res[0].balance;
    }

    // console.log(res[0])
    return result;
  }

  async getWalletBalances (params) {
    // console.log('getWalletBalances_' + this.coreType + '_' + this.tokenType);

    // prep rpc command
    var batch = [{
      method: 'omni_getwalletaddressbalances'
    }];

    var res = await this.client.command(batch);
    this.validate(res);

    const formattedBalances = [];

    for (const addressData of res[0]) {
      const address = addressData.address;

      for (const balanceData of addressData.balances) {
        if (balanceData.propertyid === this.coinPropertyId) {
          formattedBalances[address] = balanceData.balance;
        }
      }
    }

    return formattedBalances;
  }

  async getWalletTotal (params) {
    console.log('getWalletTotal_' + this.coreType + '_' + this.tokenType);

    // prep rpc command
    var batch = [{
      method: 'omni_getwalletbalances',
      parameters: []
    }];

    var res = await this.client.command(batch);
    this.validate(res);

    let walletTotal = 0;

    for (const propertyData of res[0]) {
      if (propertyData.propertyid === this.coinPropertyId) {
        walletTotal = propertyData.balance;
        return walletTotal;
      }
    }

    return walletTotal;
  }

  // Sends push tx with funder for btc fee if funder is specified.
  // funder, recipient in run.config
  async sendTransaction (params) {
    console.log('sendTransaction_' + this.coreType + '_' + this.tokenType);

    // declare
    let hash = '';
    const sender = params.sender;
    const recipient = params.recipient;
    const amount = params.amount;
    const funder = params.funder || null;

    // prep rpc command
    let batch = [{
      method: 'omni_send',
      parameters: [sender, recipient, this.coinPropertyId, amount]
    }];

    if (funder != null) {
      batch = [{
        method: 'omni_funded_send',
        parameters: [sender, recipient, this.coinPropertyId, amount, funder]
      }];
    }

    if (!this.auto_send) {
      return {
        skip: 'Send transaction is switched off by auto_send:false'
      };
    }

    // call omni_send
    var res = await this.client.command(batch);
    this.validate(res);

    hash = res[0];

    // call fetch transaction details to confirm it is recorded.
    const txParams = {
      txid: hash
    };
    var resHash = await this.getTransaction(txParams);
    this.validate(resHash);

    return hash;
  }

  async computeAddressReceivedSQL (params) {
    // console.log('computeAddressReceivedSQL_' + this.coreType + '_' + this.tokenType);

    const address = params.address;

    if (!address || typeof address !== 'string') {
      // console.log("-E- txid param not valid.")
      return {
        error: 'Please make sure that argument specified is a string.'
      };
    }

    // fetch all scheduled forward tx that are not equals to INVALID
    var totalAmt = 0;
    totalAmt = await SQL.usdt_clear_batches.sum('amount', {
      where: {
        paymentAddr: address,
        status: {
          [lib.Op.notIn]: ['INVALID', 'OPEN']
        }
      }
    });

    if (!totalAmt) {
      totalAmt = 0;
    }

    return parseFloat(totalAmt);
  }
}

// eslint-disable-next-line camelcase
module.exports = CoinCore_BTC_USDT;
