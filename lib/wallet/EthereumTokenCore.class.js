const EthereumCore = require('./EthereumCore.class.js').EthereumCore;
const lib = require('../lib.js');
const Web3 = require('web3');
const abiDecoder = require('abi-decoder');
const SQL = lib.SQL;

/*
* Parent class for ERC20 based Tokens
* Main class containing general ERC20 functions for manipulating and getting information on ERC20 tokens in the Ethereum Blockchain
* Contains general functions that can be used by all child classes
*/

class EthereumTokenCore extends EthereumCore {
  constructor () {
    super();

    // Placeholders to be overwritten by token class
    this.tokenType = 'USDT';

    // Placeholders to be overwritten by token class
    this.tokenAddress = '0xdac17f958d2ee523a2206206994597c13d831ec7';

    // Placeholders to be overwritten by token class
    this.contractABI = [
      {
        constant: true,
        inputs: [],
        name: 'name',
        outputs: [
          {
            name: '',
            type: 'string'
          }
        ],
        payable: false,
        stateMutability: 'view',
        type: 'function'
      }
    ];

    abiDecoder.addABI(this.contractABI);

    // Get ERC20 Token contract instance
    this.contract = new this.client.eth.Contract(
      this.contractABI,
      this.tokenAddress,
      {
        defaultGasPrice: '20000000000' // default gas price in wei, 20 gwei in this case
      }
    );
  }

  async convertFromWei (amount) {
    if (this.decimals === undefined) {
      this.decimals = await this.contract.methods.decimals().call();
    }

    const value = parseFloat(amount) / Math.pow(10, this.decimals.toString());
    // let value = this.client.utils.fromWei(amount.toString(), this.unitMapping[this.decimals]);

    return value.toString();
  }

  async convertToWei (amount) {
    if (this.decimals === undefined) {
      this.decimals = await this.contract.methods.decimals().call();
    }

    const weiValue = parseFloat(amount) * Math.pow(10, this.decimals.toString());
    // let weiValue = this.client.utils.toWei(amount.toString(), this.unitMapping[this.decimals]);

    return weiValue.toFixed(0);
  }

  async estimateEtherCost (params) {
    // console.log('estimateEtherCost_' + this.coreType + '_' + this.tokenType);

    const sender = params.sender;
    const recipient = params.recipient;
    const amount = params.amount;

    // Convert amount to Wei
    const weiAmount = await this.convertToWei(amount);
    const hexWeiAmount = this.client.utils.toHex(weiAmount);

    // The gas price is determined by the last few blocks median gas price. GasPrice is the wei per unit of gas
    const medianGasPrice = await this.client.eth.getGasPrice();

    // Determine the nonce
    const count = await this.client.eth.getTransactionCount(sender);

    const txObj = {
      nonce: this.client.utils.toHex(count),
      from: sender,
      to: this.tokenAddress,
      gasPrice: this.client.utils.toHex(medianGasPrice),
      data: this.contract.methods.transfer(recipient, hexWeiAmount).encodeABI()
    };

    let gasEstimate = await this.client.eth.estimateGas(txObj) * 1.1;
    gasEstimate = Math.ceil(gasEstimate);

    const totalWeiCost = this.client.utils.toBN(gasEstimate * medianGasPrice);

    const etherCost = this.client.utils.fromWei(totalWeiCost, 'ether');

    return etherCost;
  }

  async estimateFee (params) {
    // console.log('estimateFee_' + this.coreType + '_' + this.tokenType);

    const etherCost = await this.estimateEtherCost(params);

    const feeUsd = etherCost * (await this.getEthPrice('USD'));

    return feeUsd;
  }

  async getTransaction (params) {
    // console.log('getTransaction_' + this.coreType + '_' + this.tokenType);

    const txid = params.txid;

    let result = null;
    let decodedInput = null;

    // Transaction will not appear immediately. Try for certain amount of times until info is available.
    for (let i = 1; i <= 20; i++) {
      result = await this.client.eth.getTransaction(txid);

      if (result != null) {
        decodedInput = lib.abiDecoder.decodeMethod(result.input);
        // console.log("Number of tries:" + i);
        break;
      }

      await this.delay(5000);
    }

    if (result == null) {
      throw new Error('Unable to get transaction info');
    }
    // console.log(result);
    result.paymentAddr = decodedInput.params[0].value;
    result.amount = await this.convertFromWei(decodedInput.params[1].value);

    const confParams = {
      txid: txid,
      blockNumber: result.blockNumber
    };
    result.confirmations = await this.getConfirmations(confParams);

    let status = false;
    let gasUsed = null;

    if (result.confirmations > 0) {
      const txReceipt = await this.client.eth.getTransactionReceipt(txid);
      status = txReceipt.status;
      gasUsed = txReceipt.gasUsed;
    }

    result.gasUsed = gasUsed;

    result.valid = status;

    result.fee = null;

    if (gasUsed != null) {
      const gasPriceGwei = this.client.utils.fromWei(result.gasPrice, 'gwei');

      // let feeWei = this.client.utils.fromWei(this.client.utils.toBN(gasUsed).mul(this.client.utils.toBN(gasPriceGwei)), 'gwei');
      const feeWei = this.client.utils.fromWei((gasUsed * gasPriceGwei).toFixed(0), 'gwei');

      result.fee = feeWei;
    }

    return result;
  }

  async getBalance (params) {
    // console.log('getBalance_' + this.coreType + '_' + this.tokenType);

    const address = params.address || null;

    if (address == null) {
      throw new Error('No address provided');
    }

    let result = null;

    if (Array.isArray(params.address)) {
      result = [];

      const batch = new this.client.BatchRequest();

      for (const address of params.address) {
        batch.add(this.contract.methods.balanceOf(address).call.request({}, (err, res) => { if (err) throw err; }));
      }

      const balances = await batch.execute();

      let cnt = 0;

      for (const address of params.address) {
        const balance = await this.convertFromWei(balances.response[cnt]);

        result[address] = balance;

        cnt++;
      }
    } else {
      const bigBalance = await this.contract.methods.balanceOf(address).call();

      result = await this.convertFromWei(bigBalance);
    }

    return result;
  }

  async getWalletBalances (params) {
    // console.log('getWalletBalances_' + this.coreType + '_' + this.tokenType);

    const walletAddresses = await this.getWalletAddresses();

    const batch = new this.client.BatchRequest();

    for (const addrData of walletAddresses) {
      batch.add(this.contract.methods.balanceOf(addrData.paymentAddr).call.request({}, (err, res) => { if (err) throw err; }));
    }

    const result = await batch.execute();

    const formattedBalances = [];

    for (const key in walletAddresses) {
      const resultBal = result.response[key] || 0;

      const balance = await this.convertFromWei(resultBal);

      const checkSumAddress = this.client.utils.toChecksumAddress(walletAddresses[key].paymentAddr);

      formattedBalances[checkSumAddress] = balance;
    }

    return formattedBalances;
  }

  // Sends push tx with funder for btc fee if funder is specified.
  // funder, recipient in run.config
  async sendTransaction (params) {
    console.log('sendTransaction_' + this.coreType + '_' + this.tokenType);

    // declare
    const sender = params.sender;
    const recipient = params.recipient;
    const amount = params.amount;
    let gasLimit = params.gasLimit || null;

    if (!this.client.utils.isAddress(sender) || !this.client.utils.isAddress(recipient)) {
      throw new Error('Invalid address as sender or recipient');
    }

    const senderWeiBalance = await this.contract.methods.balanceOf(sender).call();
    const senderBalance = await this.convertFromWei(senderWeiBalance);

    if (Number(senderBalance === 0)) {
      throw new Error('Sender balance is 0');
    }

    if (Number(senderBalance) < Number(amount)) {
      throw new Error('Not enough funds in sender address');
    }

    // Convert amount to Wei
    const weiAmount = await this.convertToWei(amount);
    const hexWeiAmount = this.client.utils.toHex(weiAmount);

    // The gas price is determined by the last few blocks median gas price. GasPrice is the wei per unit of gas
    const medianGasPrice = await this.client.eth.getGasPrice();

    let hash = null;
    let skip = false;

    await SQL.db.transaction(async t1 => {
      let nodeNonce = null;
      let currNonce = null;
      let nonceRecord = null;

      // Determine the nonce
      if (sender === lib.CONFIG.ETH_WALLET_ADDR.WITHDRAW) {
        nonceRecord = await SQL.configs.findOne({
          where: { conf_code: 'ETH_HOT_WALLET_NONCE' },
          transaction: t1,
          lock: t1.LOCK.UPDATE
        });

        currNonce = nonceRecord.conf_value;

        nodeNonce = await this.client.eth.getTransactionCount(sender);

        if (nodeNonce > currNonce) {
          currNonce = nodeNonce;
        }
      } else {
        currNonce = await this.client.eth.getTransactionCount(sender);
      }

      const txObj = {
        nonce: this.client.utils.toHex(currNonce),
        from: sender,
        to: this.tokenAddress,
        gasPrice: this.client.utils.toHex(medianGasPrice),
        data: this.contract.methods.transfer(recipient, hexWeiAmount).encodeABI()
      };

      if (gasLimit == null) {
        gasLimit = Math.ceil(await this.client.eth.estimateGas(txObj) * 1.1);
      }

      txObj.gasLimit = gasLimit;

      const result = await SQL.ethereum_keys.findOne({
        where: {
          ek_address: sender
        }
      });

      const password = this.walletPassword;

      const account = this.client.eth.accounts.decrypt(result.ek_keyJson, password);
      // console.log(txObj);
      const signedTx = await account.signTransaction(txObj);
      // console.log(signedTx)

      if (!this.auto_send) {
        skip = true;
        return Promise.resolve();
      }

      await result.update({ ek_lastSigned: new Date() });

      this.client.eth.sendSignedTransaction(signedTx.rawTransaction, async (error, result) => {
        if (error) {
          console.log(error);

          const watchedAddr = await lib.SQL.watch_addresses.findOne({
            where: {
              wa_address: sender
            }
          });

          const user = watchedAddr.wa_userId;

          const userObj = await lib.SQL.users.findOne({
            where: {
              u_userId: user
            }
          });

          const callbackUrl = userObj.u_callbackUrl;

          if (callbackUrl == null) {
            return;
          }

          const options = {
            method: 'POST',
            uri: callbackUrl,
            headers: {
              'Content-type': 'application/json',
              Accept: 'application/json',
              'Accept-Charset': 'utf-8'
            },
            body: {
              txId: signedTx.transactionHash,
              success: false,
              error: error
            },
            json: true // Automatically parses the JSON string in the response
          };

          lib.requestpn(options).then(result => {
            return result;
          }).catch(err => {
            throw err;
          });
        } else {
          console.log(result);
        }
      });

      hash = signedTx.transactionHash;

      // //call fetch transaction details to confirm it is recorded.
      // let txParam = {
      //     txid: hash
      // }

      // var resHash = await this.getTransaction(txParam);
      // console.log(resHash)

      // if(!resHash){
      //     throw new Error("Transaction was not recorded on blockchain");
      // }

      if (sender === lib.CONFIG.ETH_WALLET_ADDR.WITHDRAW) {
        await nonceRecord.update({
          conf_value: parseFloat(currNonce) + 1
        }, { transaction: t1 });
      }

      return Promise.resolve();
    }).then(hash => {

    }).catch(err => {
      throw err;
    });

    if (skip) {
      return {
        skip: 'Send transaction is switched off by auto_send:false'
      };
    }

    return hash;
  }

  async computeAddressReceivedSQL (params) {
    // console.log('computeAddressReceivedSQL_' + this.coreType + '_' + this.tokenType);

    const address = params.address;

    if (!address || typeof address !== 'string') {
      // console.log("-E- txid param not valid.")
      return {
        error: 'Please make sure that argument specified is a string.'
      };
    }

    // fetch all scheduled forward tx that are not equals to INVALID
    var totalAmt = 0;
    totalAmt = await SQL.usdt_clear_batches.sum('amount', {
      where: {
        node: this.coreType,
        token: this.tokenType,
        paymentAddr: address,
        status: {
          [lib.Op.notIn]: ['INVALID', 'OPEN']
        }
      }
    });

    if (!totalAmt) {
      totalAmt = 0;
    }

    return totalAmt;
  }

  async watchPendingTx (params) {
    // Instantiate web3 with WebSocket provider
    const subWeb3 = new Web3(new Web3.providers.WebsocketProvider(this.rpcWebSocketUrl));

    // Instantiate token contract object with JSON ABI and address
    const tokenContract = new subWeb3.eth.Contract(
      this.contractABI, this.tokenAddress,
      (error, result) => { if (error) throw error; }
    );

    // Generate filter options
    const options = {
      // filter: {
      // _from:  process.env.WALLET_FROM,
      // _to:    process.env.WALLET_TO,
      // _value: process.env.AMOUNT
      // },
      fromBlock: 'latest'
    };
    // const checkedAddr = {};
    // Subscribe to Transfer events matching filter criteria
    tokenContract.events.Transfer(options, async (error, event) => {
      if (error) {
        throw error;
      }

      const watchedAddress = await lib.SQL.watch_addresses.findOne({
        where: {
          [lib.Op.or]: [{ wa_address: event.returnValues.from }, { wa_address: event.returnValues.to }]
        }
      });

      // Do nothing if transaction not linked to watched address
      if (watchedAddress === null) {
        return;
      }

      // Convert from Wei to Token Value
      const amount = await this.convertFromWei(event.returnValues.tokens.toString() || event.returnValues.value.toString());

      const trx = await this.client.eth.getTransaction(event.transactionHash);

      // Will use Sequelize defined unique key to search if row exists. (In this case tx_transactionId)
      await lib.SQL.transactions.upsert({
        tx_node: this.coreType,
        tx_token: this.tokenType,
        tx_transactionId: event.transactionHash,
        tx_status: 'PENDING',
        tx_amount: amount,
        tx_senderAddr: event.returnValues.from,
        tx_recipientAddr: event.returnValues.to,
        tx_ethGasPrice: trx.gasPrice,
        tx_ethNonce: trx.nonce
      });

      const existingTx = await lib.SQL.transactions.findOne({
        where: {
          tx_transactionId: event.transactionHash
        }
      });

      // Initiate transaction confirmation
      this.confirmTransaction(existingTx);
    });
  }
}

module.exports = EthereumTokenCore;
