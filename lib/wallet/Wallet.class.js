/*
* Main Class to consolidate all blockchain transaction related functions
* Instantiates all supported token cores to allow a central class and function to be used to call token specific functions
*/

class Wallet {
  constructor () {
    this.arrCoinCores = {};

    this.validCoins = ['BTC_BTC', 'BTC_USDT', 'ETH_ETH', 'ETH_USDT'];

    for (const coinType of this.validCoins) {
      this.arrCoinCores[coinType] = this.createCore(coinType);
    }
  }

  createCore (coinType) {
    const [node, token] = coinType.split('_');

    if (node === 'BTC') {
      const classpath = './CoinCore_' + coinType + '.class.js';
      const CoinCore = require(classpath);
      return new CoinCore();
    } else {
      if (token === 'ETH') {
        const classpath = './EthereumCore.class.js';
        const EthCore = require(classpath);
        return new EthCore.EthereumCore();
      }

      const classpath = './EthToken_' + token + '.class.js';
      const Token = require(classpath);
      return new Token();
    }
  }

  async getAllBalances (params) {
    if (!Array.isArray(params.addresses)) {
      throw new Error('Invalid argument, expect array of addresses.');
    }

    const balances = [];

    // Use array of addresses and create object with addresses as keys
    for (const key in params.addresses) {
      balances[params.addresses[key]] = {};
    }

    for (const coinType of this.validCoins) {
      // Initialize balances of this coin type to 0
      for (const address in balances) {
        balances[address][coinType] = 0;
      }

      const params = {
        coinType: coinType,
        address: null
      };
      const coinBalances = await this.arrCoinCores[coinType].getWalletBalances(params);

      // console.log(coinBalances);

      for (const address in coinBalances) {
        if (balances[address]) {
          balances[address][coinType] = coinBalances[address];
        }
      }
    }

    // console.log(balances);

    return balances;
  }

  async getTokenType (params) {
    const node = params.node;
    const txid = params.txid;

    const nodeCores = [];

    for (const coreName in this.arrCoinCores) {
      if (coreName.toUpperCase().includes(node)) {
        nodeCores.push(this.arrCoinCores[coreName]);
      }
    }

    for (const core of nodeCores) {
      params = {
        node: node,
        txid: txid
      };

      const result = await core.getTokenType(params);

      if (result !== false) {
        return result;
      }
    }

    return false;
  }

  async getFeePrice (params) {
    const [node] = params.coinType.split('_');

    if (node === 'ETH') {
      return this.arrCoinCores[params.coinType].getEthPrice();
    } else {
      return this.arrCoinCores[params.coinType].getBtcPrice();
    }
  }

  async getBtcPrice (params) {
    return this.arrCoinCores[params.coinType].getBtcPrice();
  }

  async getEthPrice (params) {
    return this.arrCoinCores[params.coinType].getEthPrice();
  }

  async getBlockInfo (params) {
    return this.arrCoinCores[params.coinType].getBlockInfo(params);
  }

  async estimateEtherCost (params) {
    return this.arrCoinCores[params.coinType].estimateEtherCost(params);
  }

  async estimateFee (params) {
    return this.arrCoinCores[params.coinType].estimateFee(params);
  }

  async getPrivateKey (params) {
    return this.arrCoinCores[params.coinType].getPrivateKey(params);
  }

  async importPrivateKey (params) {
    return this.arrCoinCores[params.coinType].importPrivateKey(params);
  }

  async createAcc (params) {
    return this.arrCoinCores[params.coinType].createAcc(params);
  }

  async validateAddr (params) {
    return this.arrCoinCores[params.coinType].validateAddr(params);
  }

  async getTransaction (params) {
    return this.arrCoinCores[params.coinType].getTransaction(params);
  }

  async getPendingTx (params) {
    return this.arrCoinCores.BTC_USDT.getPendingTx(params);
  }

  async listTransactions (params) {
    return this.arrCoinCores[params.coinType].listTransactions(params);
  }

  async listAccounts (params) {
    return this.arrCoinCores[params.coinType].listAccounts(params);
  }

  async getBalance (params) {
    return this.arrCoinCores[params.coinType].getBalance(params);
  }

  async getWalletBalances (params) {
    return this.arrCoinCores[params.coinType].getWalletBalances(params);
  }

  async getWalletTotal (params) {
    return this.arrCoinCores[params.coinType].getWalletTotal(params);
  }

  async sendTransaction (params) {
    return this.arrCoinCores[params.coinType].sendTransaction(params);
  }

  async computeAddressReceivedSQL (params) {
    return this.arrCoinCores[params.coinType].computeAddressReceivedSQL(params);
  }

  async watchPendingTx (params) {
    return this.arrCoinCores[params.coinType].watchPendingTx(params);
  }
}

module.exports = Wallet;
