const CoinCore = require('./CoinCore.class.js').CoinCore;
const lib = require('./CoinCore.class.js').lib;
const utility = require('./CoinCore.class.js').utility;
// const bitcoin = require('bitcoinjs-lib')
// const net = bitcoin.networks.bitcoin
// const SQL = lib.SQL
const BLOCK_INFO = 'https://blockchain.info';

/*
* Coin Core for Bitcoin
* Contains all functions related to manipulating and getting information on Bitcoin in the Bitcoin blockchain
*/

// eslint-disable-next-line camelcase
class CoinCore_BTC_BTC extends CoinCore {
  constructor () {
    super();

    this.tokenType = 'BTC';
  }

  validate (result) {
    if (result === undefined) {
      return {
        error: 'Please specify rpc result in argument.'
      };
    }
    if (JSON.stringify(result).toLowerCase().includes('rpcerror')) {
      return {
        error: '-E- ' + result
      };
    } else {
      return result;
    }
  }

  // estimate fee bitcoin fees in bytes
  // default 405 bytes per transaction for (funding type)
  // default estimate fee for transaction to be mined within 6 blocks (10mins per block)
  async estimateFee (params) {
    console.log('estimateFee_' + this.coreType + '_' + this.tokenType);

    const size = params.size || 405;
    const blocks = params.blocks || 6;

    // prep rpc command
    var batch = [{
      method: 'estimatesmartfee',
      parameters: [blocks]
    }];

    var res = await this.client.command(batch);

    if (this.validate(res).error) {
      return this.validate(res);
    }
    var feeBtc = res[0].feerate * (size / 1000);
    var btcPrice = await this.getBtcPrice();
    var feeUsd = parseFloat(feeBtc * btcPrice).toFixed(2);

    return feeUsd;
  }

  async getTransaction (params) {
    console.log('getTransaction_' + this.coreType + '_' + this.tokenType);

    const txid = params.txid;

    // prep rpc command
    var batch = [{
      method: 'gettransaction',
      parameters: [txid]
    }];

    var res = await this.client.command(
      batch);
    if (this.validate(res).error) {
      return this.validate(res);
    }

    // console.log(res[0])
    return res[0];
  }

  async getPendingTx (params) {
    console.log('getPendingTx_' + this.coreType + '_' + this.tokenType);

    // const address = params.address
  }

  // address needs to be from wallet.
  // Returns complete transactions for wallets that are generated and imported to wallet.dat since usage.
  async listTransactions (params) {
    console.log('listTransactions_' + this.coreType + '_' + this.tokenType);

    const label = params.address;

    // prep rpc command
    var batch = [{
      method: 'listtransactions',
      parameters: [label]
    }];

    var res = await this.client.command(batch);
    if (this.validate(res).error) {
      return this.validate(res);
    }

    // console.log(res[0])
    return res[0];
  }

  async getBalance (params) {
    // console.log('getBalance_' + this.coreType + '_' + this.tokenType);

    const address = params.address || null;

    if (address == null) {
      throw new Error('No address provided');
    }

    let result = null;
    let statusCode = 0;
    let balObj;

    if (Array.isArray(params.address)) {
      var command = `${BLOCK_INFO}/balance?active=`;

      for (const addr of params.address) {
        command += addr + '|';
      }

      do {
        // Reset status code to 0 after requesting again
        statusCode = 0;

        balObj = await lib.requestpn(
          command
        )
          .then(JSON.parse).catch(async e => {
            // Status Code 429 occurs when there are too many requests. Try again until succeed if status code is 429
            statusCode = e.statusCode || 0;

            if (statusCode !== 429) {
              return {
                error: e.message
              };
            }

            // Wait a few seconds before requesting again
            await this.delay(3000);
          });
      }
      while (statusCode === 429);

      if (balObj.constructor === Object) {
        result = {};
        // convert satoshi to btc
        for (const addr of params.address) {
          result[addr] = this.convertSatoshiToBtc(balObj[addr].final_balance);
        }

        return result;
      }
    } else {
      do {
        // Reset status code to 0 after requesting again
        statusCode = 0;

        balObj = await lib.requestpn(
                    `${BLOCK_INFO}/balance?active=` + params.address
        )
          .then(JSON.parse).catch(async e => {
            // Status Code 429 occurs when there are too many requests. Try again until succeed if status code is 429
            statusCode = e.statusCode || 0;

            if (statusCode !== 429) {
              return {
                error: e.message
              };
            }

            // Wait a few seconds before requesting again
            await this.delay(3000);
          });
      }
      while (statusCode === 429);

      if (balObj.constructor === Object) {
        result = this.convertSatoshiToBtc(balObj[params.address].final_balance);

        return result;
      }
    }

    // console.log(res[0])
    return result;
  }

  async getWalletBalances (params) {
    // console.log('getWalletBalances_' + this.coreType + '_' + this.tokenType);

    var batch = [{
      method: 'listreceivedbyaddress',
      parameters: []
    }];

    var res = await this.client.command(batch);
    if (this.validate(res).error) {
      return this.validate(res);
    }

    // console.log(res[0])
    return res[0];
  }

  async getWalletTotal (params) {
    console.log('getWalletTotal_' + this.coreType + '_' + this.tokenType);

    // prep rpc command
    var batch = [{
      method: 'getbalance',
      parameters: []
    }];

    var res = await this.client.command(batch);
    if (this.validate(res).error) {
      return this.validate(res);
    }

    // console.log(res[0])
    return res[0];
  }

  async sendTransaction (params) {
    console.log('sendTransaction_' + this.coreType + '_' + this.tokenType);

    const sender = params.sender;
    const recipient = params.recipient;
    const amount = params.amount;

    // prep rpc command
    var batch = [{
      method: 'listunspent',
      parameters: [sender, recipient, amount]
    }];

    var res = await this.client.command(batch);
    if (this.validate(res).error) {
      return this.validate(res);
    }

    const recipientUnspent = res[0].filter(
      (txObj) => (
        txObj.address === recipient &&
                txObj.spendable === true &&
                txObj.solvable === true
      )
    );

    const sortedRecipientUnspent = recipientUnspent.sort(utility.objArrSortFunc('amount', 'desc'));

    let remainder = 0;
    let amountLeftover = amount;
    const inputs = [];

    for (const unspent of sortedRecipientUnspent) {
      inputs.push({
        txid: unspent.txid,
        vout: unspent.vout
      });

      remainder = unspent.amount - amountLeftover;

      amountLeftover -= unspent.amount;

      if (amountLeftover <= 0) {
        break;
      }
    }

    if (amountLeftover > 0) {
      throw new Error('Insufficient Unspents');
    }

    let outputs = {

      // A key-value pair. The key (string) is the bitcoin address, the value (float or string) is the amount in BTC
      [recipient]: amount, // Output to send amount to recipient address
      [sender]: remainder // Output to send remainder back to sender wallet

    };

    batch = [{
      method: 'createrawtransaction',
      parameters: [inputs, outputs]
    }];

    res = await this.client.command(batch);
    if (this.validate(res).error) {
      return this.validate(res);
    }

    const estRawTransaction = res[0];

    const privateKey = await this.getPrivateKey({ address: sender });

    batch = [{
      method: 'signrawtransactionwithkey',
      parameters: [estRawTransaction, [privateKey]]
    }];

    res = await this.client.command(batch);
    if (this.validate(res).error) {
      return this.validate(res);
    }

    const estSignedTx = res[0];

    batch = [{
      method: 'decoderawtransaction',
      parameters: [estSignedTx.hex]
    }];

    res = await this.client.command(batch);
    if (this.validate(res).error) {
      return this.validate(res);
    }

    const finalSize = res[0].size;

    batch = [{
      method: 'estimatesmartfee',
      parameters: [6]
    }];

    res = await this.client.command(batch);
    if (this.validate(res).error) {
      return this.validate(res);
    }

    const feeRate = res[0].feerate / 1024; // feeRate is per kb while our size is in bytes

    const transactionFee = Number((finalSize * feeRate).toFixed(8));

    // Outputs minus Inputs = transaction fee | Therefore deduct transaction fee from remainder to send back to sender
    remainder = remainder - transactionFee;

    outputs = {

      // A key-value pair. The key (string) is the bitcoin address, the value (float or string) is the amount in BTC
      [recipient]: amount, // Output to send amount to recipient address
      [sender]: remainder // Output to send remainder back to sender wallet

    };

    batch = [{
      method: 'createrawtransaction',
      parameters: [inputs, outputs]
    }];

    res = await this.client.command(batch);
    if (this.validate(res).error) {
      return this.validate(res);
    }

    const finalRawTx = res[0];

    batch = [{
      method: 'signrawtransactionwithkey',
      parameters: [finalRawTx, [privateKey]]
    }];

    res = await this.client.command(batch);
    if (this.validate(res).error) {
      return this.validate(res);
    }

    // const finalSignedTx = res[0];

    // batch = [{
    //     method: "sendrawtransaction",
    //     parameters: [finalRawTx]
    // }];

    // res = await this.client.command(batch);

    return res[0];
  }

  async computeAddressReceivedSQL (params) {
    console.log('computeAddressReceivedSQL_' + this.coreType + '_' + this.tokenType);
  }
}

// eslint-disable-next-line camelcase
module.exports = CoinCore_BTC_BTC;
