function currTime () {
//   const city = 'Singapore'
  const offset = '+8';

  const d = new Date();
  const utc = d.getTime() + (d.getTimezoneOffset() * 60000);
  const nd = new Date(utc + (3600000 * offset));
  let datetime = nd.toLocaleString();
  datetime = datetime.replace(' AM', 'AM: ');
  datetime = datetime.replace(' PM', 'PM: ');
  datetime = datetime.replace(' ', '');

  return datetime;
}

module.exports = {
  debug: async function (message) {
    console.log('-D- ' + currTime() + message);
  }, // end of debug

  info: async function (message) {
    console.log('-I- ' + currTime() + message);
  }, // end of log

  error: async function (message) {
    console.log('-E- ' + currTime() + message);
  } // end of error

}; // end of module.exports
