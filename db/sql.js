/* eslint-disable camelcase */
const lib = require('../lib/lib.js');
const Sequelize = require('sequelize');

const sequelize_config = {
  host: lib.CONFIG.DB.HOST,
  maxConcurrentQueries: 100,
  logging: false,
  dialect: 'postgres',
  dialectOptions: {
    ssl: 'Amazon RDS'
  },
  pool: {
    maxConnections: 10,
    maxIdleTime: 15
  },
  language: 'en',
  freezeTableName: true
};

if (lib.CONFIG.DB.HOST === 'localhost') {
  delete sequelize_config.dialectOptions;
}

const db = new Sequelize(lib.CONFIG.DB.SCHEMA, lib.CONFIG.DB.USER, lib.CONFIG.DB.PASSWORD, sequelize_config);

// const api_keys = db.define('api_keys', {
//   api_id: {
//     type: sequelize.BIGINT,
//     primaryKey: true,
//     autoIncrement: true
//   },
//   api_isActive: sequelize.BOOLEAN,
//   api_userId: sequelize.STRING,
//   api_label: sequelize.STRING,
//   api_apiKey: sequelize.STRING,
//   api_secretKey: sequelize.STRING
// }, {
//   timestamps: true,
//   createdAt: 'api_createdAt',
//   updatedat: 'api_updatedat'
// })

// const whitelisted_ips = db.define('whitelisted_ips', {
//   wip_id: {
//     type: sequelize.BIGINT,
//     primaryKey: true,
//     autoIncrement: true
//   },
//   wip_userId: sequelize.STRING,
//   wip_apiKey: sequelize.STRING,
//   wip_ip: sequelize.STRING,
//   wip_lastAccess: sequelize.DATE
// }, {
//   timestamps: true,
//   createdAt: 'wip_createdAt',
//   updatedat: 'wip_updatedat'
// })

// const users = db.define('users', {
//   u_id: {
//     type: sequelize.BIGINT,
//     primaryKey: true,
//     autoIncrement: true
//   },
//   u_userId: sequelize.STRING,
//   u_email: sequelize.STRING,
//   u_name: sequelize.STRING,
//   u_callbackUrl: sequelize.STRING,
//   u_isActive: sequelize.BOOLEAN
// }, {
//   timestamps: true,
//   createdAt: 'u_createdAt',
//   updatedat: 'u_updatedat'
// })

const configs = db.define('configs', {
  conf_code: {
    type: Sequelize.STRING,
    primaryKey: true
  },
  conf_cat1: Sequelize.STRING,
  conf_cat2: Sequelize.STRING,
  conf_cat3: Sequelize.STRING,
  conf_value: Sequelize.STRING
}, {
  timestamps: true,
  createdAt: 'conf_createdAt',
  updatedat: 'conf_updatedat'
});

const telegram_bots = db.define('telegram_bots', {
  update_id: Sequelize.INTEGER
});

const logs = db.define('logs', {
  log_id: {
    type: Sequelize.BIGINT,
    primaryKey: true,
    autoIncrement: true
  },
  log_type: Sequelize.STRING,
  log_cat1: Sequelize.STRING,
  log_cat2: Sequelize.STRING,
  log_cat3: Sequelize.STRING,
  log_message: Sequelize.TEXT,
  log_error: Sequelize.TEXT
}, {
  timestamps: true,
  createdAt: 'log_createdAt',
  updatedat: 'log_updatedat'
});

// const user_activity_logs = db.define('user_activity_logs', {
//   ual_id: {
//     type: sequelize.BIGINT,
//     primaryKey: true,
//     autoIncrement: true
//   },
//   ual_userId: Sequelize.STRING,
//   ual_category: Sequelize.STRING,
//   ual_message: Sequelize.STRING,
//   ual_rowId: Sequelize.STRING,
//   ual_table: Sequelize.STRING,
//   ual_col: Sequelize.STRING,
//   ual_oldVal: Sequelize.TEXT,
//   ual_newVal: Sequelize.TEXT
// }, {
//   timestamps: true,
//   createdAt: 'ual_createdAt',
//   updatedat: 'ual_updatedat'
// })

// const ethereum_keys = db.define('ethereum_keys', {
//   ek_pkey: {
//     type: Sequelize.BIGINT,
//     primaryKey: true,
//     autoIncrement: true
//   },
//   ek_userId: Sequelize.STRING,
//   ek_version: Sequelize.STRING,
//   ek_id: Sequelize.STRING,
//   ek_address: Sequelize.STRING,
//   ek_keyJson: Sequelize.TEXT,
//   ek_lastSigned: Sequelize.BOOLEAN
// }, {
//   timestamps: true,
//   createdAt: 'ek_createdAt',
//   updatedat: 'ek_updatedat'
// })

const watch_addresses = db.define('watch_addresses', {
  wa_id: {
    type: Sequelize.BIGINT,
    primaryKey: true,
    autoIncrement: true
  },
  wa_userId: Sequelize.STRING,
  wa_address: Sequelize.STRING,
  wa_nodeType: Sequelize.STRING,
  wa_status: Sequelize.STRING
}, {
  timestamps: true,
  createdAt: 'wa_createdAt',
  updatedat: 'wa_updatedat'
});

const transactions = db.define('transactions', {
  tx_id: {
    type: Sequelize.BIGINT,
    primaryKey: true,
    autoIncrement: true
  },
  tx_nodeType: Sequelize.STRING,
  tx_token: Sequelize.STRING,
  tx_transactionId: {
    type: Sequelize.STRING,
    unique: true
  },
  tx_status: Sequelize.STRING,
  tx_confirmedAt: Sequelize.DATE,
  tx_amount: Sequelize.DECIMAL,
  tx_senderAddr: Sequelize.STRING,
  tx_recipientAddr: Sequelize.STRING,
  tx_funderAddr: Sequelize.STRING,
  tx_funderTxId: Sequelize.STRING,
  tx_networkFee: Sequelize.DECIMAL,
  tx_networkFeeUsd: Sequelize.DECIMAL,
  tx_ethGasUsed: Sequelize.INTEGER,
  tx_ethGasPrice: Sequelize.INTEGER,
  tx_ethNonce: Sequelize.INTEGER
}, {
  timestamps: true,
  createdAt: 'tx_createdAt',
  updatedat: 'tx_updatedat'
});

const eth_fund_txs = db.define('eth_fund_txs', {
  eft_id: {
    type: Sequelize.BIGINT,
    primaryKey: true,
    autoIncrement: true
  },
  eft_userId: Sequelize.STRING,
  eft_type: Sequelize.STRING,
  eft_token: Sequelize.STRING,
  eft_amount: Sequelize.DECIMAL,
  eft_paymentAddr: Sequelize.STRING,
  eft_status: Sequelize.STRING,
  eft_txid: {
    type: Sequelize.STRING,
    unique: true,
    // This is a reference to another model
    model: transactions,
    // This is the column name of the referenced model
    key: 'tx_id'
  },
  eft_sendFee: Sequelize.DECIMAL,
  eft_sendFeeUsd: Sequelize.DECIMAL,
  eft_ethGasUsed: Sequelize.INTEGER,
  eft_ethGasPrice: Sequelize.INTEGER,
  eft_ethNonce: Sequelize.INTEGER,
  eft_confirmedAt: Sequelize.DATE
}, {
  timestamps: true,
  createdAt: 'eft_createdAt',
  updatedat: 'eft_updatedat'
});

db.sync({ match: /postgres$/ });

// module.exports.api_keys = api_keys;
// module.exports.whitelisted_ips = whitelisted_ips;
// module.exports.users = users;
module.exports.configs = configs;
module.exports.telegram_bots = telegram_bots;
module.exports.logs = logs;
// module.exports.user_activity_logs = user_activity_logs;
// module.exports.ethereum_keys = ethereum_keys;
module.exports.watch_addresses = watch_addresses;
module.exports.transactions = transactions;
module.exports.eth_fund_txs = eth_fund_txs;

module.exports.db = db;
