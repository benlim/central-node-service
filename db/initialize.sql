CREATE TABLE logs (
    log_id int NOT NULL auto_increment primary KEY,
    log_type varchar(255),
    log_cat1 varchar(255),
    log_cat2 varchar(255),
    log_cat3 varchar(255),
    log_message text,
    log_error text,
    log_createdAt int default current_timestamp
);

CREATE TABLE configs (
    conf_code varchar(255) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    conf_cat1 varchar(255),
    conf_cat2 varchar(255),
    conf_cat3 varchar(255),
    conf_value varchar(255)
);

create table users (
	u_id INTEGER primary key,
	u_role varchar(255),
	u_userId varchar(255) UNIQUE,
	u_password varchar(255),
	u_email varchar(255),
	u_name varchar(255),
	u_date_of_birth varchar(255),
	u_contactNo varchar(255),
	u_address varchar(255),
	u_postcode varchar(255),
	u_state varchar(255),
	u_country varchar(255),
	u_isActive BOOLEAN,
	u_createdAt DATETIME default current_timestamp,
	u_updatedAt DATETIME
);

CREATE TABLE user_activity_logs (
	ual_id int(11) AUTO_INCREMENT PRIMARY KEY,
	ual_userId varchar(255),
	ual_category varchar(255),
	ual_message varchar(255),
	ual_rowId varchar(255),
	ual_table varchar(255),
	ual_col varchar(255),
	ual_oldVal varchar(255),
	ual_newVal varchar(255),
	ual_createdAt datetime DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT user_activity_logs_userId FOREIGN KEY (ual_userId)  REFERENCES users(u_userId)
);

-- Optional
CREATE TABLE api_keys (
    api_id int NOT null AUTO_INCREMENT PRIMARY KEY,
    api_isActive BOOLEAN,
    api_userId varchar(255),
    api_label varchar(255),
    api_apiKey varchar(255) UNIQUE,
    api_secretKey varchar(255),
    api_createdAt datetime DEFAULT CURRENT_TIMESTAMP,
    api_updatedAt datetime,
    CONSTRAINT api_keys_userId FOREIGN KEY (api_userId)  REFERENCES users(u_userId)
);

-- Optional
    CREATE TABLE whitelisted_ips (
    wip_id int NOT NULL auto_increment PRIMARY KEY,
    wip_userId varchar(255),
    wip_apiKey varchar(255),
    wip_ip varchar(255),
    wip_lastAccess datetime,
    wip_createdAt datetime default current_timestamp,
    CONSTRAINT whitelisted_ips_userId FOREIGN KEY (wip_userId)  REFERENCES users(u_userId),
    CONSTRAINT whitelisted_ips_apiKey FOREIGN KEY (wip_apiKey)  REFERENCES api_keys(api_apiKey)
);

