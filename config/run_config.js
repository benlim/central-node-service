const path = require('path');

var isWindows = process.platform === 'win32';

if (isWindows) {
  require('dotenv').config({ path: path.join(__dirname, '\\.env') });
} else {
  require('dotenv').config({ path: path.join(__dirname, '/.env') });
}

module.exports = {
  // DEVELOPMENT
  dev: {
    MAIN: process.env.DEV_MAIN_URL,
    IP: process.env.DEV_MAIN_IP,
    DOMAIN: process.env.DEV_MAIN_DOMAIN,
    PORT: process.env.DEV_MAIN_PORT,
    RUN: 'development',
    PREFIX: process.env.DEV_PREFIX,
    DB: {
      SCHEMA: process.env.DEV_DB_SCHEMA,
      HOST: process.env.DEV_DB_HOST,
      USER: process.env.DEV_DB_USER,
      PASSWORD: process.env.DEV_DB_PASSWORD
    },
    EMAIL_SERVER: {
      HOST: process.env.DEV_EMAIL_SERVER_HOST,
      PORT: process.env.DEV_EMAIL_SERVER_PORT,
      FROM: '"Template.io Support" <noreply@template.io>',
      ADDRESS: 'noreply@template.io',
      PASSWORD: process.env.DEV_EMAIL_SERVER_PASSWORD
    },
    BTC_NODE: {
      NETWORK: process.env.DEV_BTC_NODE_NETWORK,
      HOST: process.env.DEV_BTC_NODE_HOST,
      PORT: process.env.DEV_BTC_NODE_PORT,
      USER: process.env.DEV_BTC_NODE_USER,
      PASSWORD: process.env.DEV_BTC_NODE_PASSWORD
    },
    ETH_NODE: {
      NETWORK: process.env.DEV_ETH_NODE_NETWORK,
      WEB_SOCKET: process.env.DEV_ETH_NODE_WEB_SOCKET
    },
    BTC_WALLET_ADDR: {
      FUNDER: process.env.DEV_BTC_WALLET_ADDR_FUNDER,
      WITHDRAW: process.env.DEV_BTC_WALLET_ADDR_WITHDRAW,
      DESTADDR: process.env.DEV_BTC_WALLET_ADDR_DESTADDR
    },
    ETH_WALLET_ADDR: {
      WITHDRAW: process.env.DEV_ETH_WALLET_ADDR_WITHDRAW,
      COLD: process.env.DEV_ETH_WALLET_ADDR_COLD
    },
    ETH_WALLET_PASSWORD: process.env.DEV_ETH_WALLET_PASSWORD,
    SALT_ROUNDS: 10,
    JWT: {
      SECRET: process.env.DEV_JWT_SECRET
    },
    ENDPOINTS: {
      PUBLIC_URLS: [
        // process.env.DEV_PREFIX + "/merchant/register",
        // process.env.DEV_PREFIX + "/merchant/login",
        // process.env.DEV_PREFIX + "/merchant/login/forgot",
        // process.env.DEV_PREFIX + "/merchant/reset/info",
        // process.env.DEV_PREFIX + "/merchant/reset/verify",
        // process.env.DEV_PREFIX + "/merchant/verify",
        // process.env.DEV_PREFIX + "/merchant/reset/password",
        // process.env.DEV_PREFIX + "/test/ipn_listener"
      ],
      PRIVATE_URLS: [
        // process.env.DEV_PREFIX + "/users",
        // process.env.DEV_PREFIX + "/users/login",
        // process.env.DEV_PREFIX + "/users/verify"
      ],
      APIKEY_URLS: [
        process.env.DEV_PREFIX + '/watch',
        process.env.DEV_PREFIX + '/wallet/balances',
        process.env.DEV_PREFIX + '/wallet/addresses',
        process.env.DEV_PREFIX + '/wallet/addresses/validate',
        process.env.DEV_PREFIX + '/wallet/keys',
        process.env.DEV_PREFIX + '/wallet/transactions',
        process.env.DEV_PREFIX + '/wallet/transactions/pending',
        process.env.DEV_PREFIX + '/wallet/estimateEthCost',
        process.env.DEV_PREFIX + '/wallet/send',
        process.env.DEV_PREFIX + '/wallet/send/funded',
        process.env.DEV_PREFIX + '/import/transaction'

      ],
      BLOCK_PATHS: [
        // "/merchant/api"
      ]
    },
    ETHERSCAN: {
      APIKEY: process.env.PROD_ETHERSCAN_APIKEY
    },
    TELEGRAM: { // DEC and PROD should have different TELEGRAM Bot and Group.
      STATUS: true,
      CHANNEL: '-1001298542296',
      GROUP: '-346186590',
      KEY: process.env.DEV_TELEGRAM_KEY
    },
    AWS: {
      APIKEY: process.env.DEV_AWS_APIKEY,
      SECRETKEY: process.env.DEV_AWS_SECRETKEY,
      BUCKET_NAME: 'template_bucket'
    },
    AES: {
      KEY: process.env.DEV_AES_KEY,
      INITIAL_VECTOR: process.env.DEV_AES_INITIAL_VECTOR
    }
  }, // end of DEV

  prod: {
    MAIN: process.env.PROD_MAIN_URL,
    IP: process.env.PROD_MAIN_IP,
    DOMAIN: process.env.PROD_MAIN_DOMAIN,
    PORT: process.env.PROD_MAIN_PORT,
    RUN: 'production',
    PREFIX: process.env.PROD_PREFIX,
    DB: {
      SCHEMA: process.env.PROD_DB_SCHEMA,
      HOST: process.env.PROD_DB_HOST,
      USER: process.env.PROD_DB_USER,
      PASSWORD: process.env.PROD_DB_PASSWORD
    },
    EMAIL_SERVER: {
      HOST: process.env.PROD_EMAIL_SERVER_HOST,
      PORT: process.env.PROD_EMAIL_SERVER_PORT,
      FROM: '"Template.io Support" <noreply@template.io>',
      ADDRESS: 'noreply@template.io',
      PASSWORD: process.env.PROD_EMAIL_SERVER_PASSWORD
    },
    BTC_NODE: {
      NETWORK: process.env.PROD_BTC_NODE_NETWORK,
      HOST: process.env.PROD_BTC_NODE_HOST,
      PORT: process.env.PROD_BTC_NODE_PORT,
      USER: process.env.PROD_BTC_NODE_USER,
      PASSWORD: process.env.PROD_BTC_NODE_PASSWORD
    },
    ETH_NODE: {
      NETWORK: process.env.PROD_ETH_NODE_NETWORK,
      WEB_SOCKET: process.env.PROD_ETH_NODE_WEB_SOCKET
    },
    BTC_WALLET_ADDR: {
      FUNDER: process.env.PROD_BTC_WALLET_ADDR_FUNDER,
      WITHDRAW: process.env.PROD_BTC_WALLET_ADDR_WITHDRAW,
      DESTADDR: process.env.PROD_BTC_WALLET_ADDR_DESTADDR
    },
    ETH_WALLET_ADDR: {
      WITHDRAW: process.env.PROD_ETH_WALLET_ADDR_WITHDRAW,
      COLD: process.env.PROD_ETH_WALLET_ADDR_COLD
    },
    ETH_WALLET_PASSWORD: process.env.PROD_ETH_WALLET_PASSWORD,
    SALT_ROUNDS: 10,
    JWT: {
      SECRET: process.env.PROD_JWT_SECRET
    },
    ENDPOINTS: {
      PUBLIC_URLS: [
        // process.env.PROD_PREFIX + "/merchant/register",
        // process.env.PROD_PREFIX + "/merchant/login",
        // process.env.PROD_PREFIX + "/merchant/login/forgot",
        // process.env.PROD_PREFIX + "/merchant/reset/info",
        // process.env.PROD_PREFIX + "/merchant/reset/verify",
        // process.env.PROD_PREFIX + "/merchant/verify",
        // process.env.PROD_PREFIX + "/merchant/reset/password",
        // process.env.PROD_PREFIX + "/test/ipn_listener"
      ],
      PRIVATE_URLS: [
        // process.env.PROD_PREFIX + "/users",
        // process.env.PROD_PREFIX + "/users/login",
        // process.env.PROD_PREFIX + "/users/verify"
      ],
      APIKEY_URLS: [
        process.env.PROD_PREFIX + '/watch',
        process.env.PROD_PREFIX + '/wallet/balances',
        process.env.PROD_PREFIX + '/wallet/addresses',
        process.env.PROD_PREFIX + '/wallet/addresses/validate',
        process.env.PROD_PREFIX + '/wallet/keys',
        process.env.PROD_PREFIX + '/wallet/transactions',
        process.env.PROD_PREFIX + '/wallet/transactions/pending',
        process.env.PROD_PREFIX + '/wallet/estimateEthCost',
        process.env.PROD_PREFIX + '/wallet/send',
        process.env.PROD_PREFIX + '/wallet/send/funded',
        process.env.PROD_PREFIX + '/import/transaction'
      ],
      BLOCK_PATHS: [
        // "/merchant/api"
      ]
    },
    ETHERSCAN: {
      APIKEY: process.env.PROD_ETHERSCAN_APIKEY
    },
    TELEGRAM: {
      STATUS: true,
      CHANNEL: '-1001298542296',
      GROUP: '-1001317185237',
      // 'GROUP': '-254782999',
      KEY: process.env.PROD_TELEGRAM_KEY
    },
    AWS: {
      APIKEY: process.env.PROD_AWS_APIKEY,
      SECRETKEY: process.env.PROD_AWS_SECRETKEY,
      BUCKET_NAME: 'template_bucket'
    },
    AES: {
      KEY: process.env.PROD_AES_KEY,
      INITIAL_VECTOR: process.env.PROD_AES_INITIAL_VECTOR
    }
  } // end of PROD
};
